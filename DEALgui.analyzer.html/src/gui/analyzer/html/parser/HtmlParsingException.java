package gui.analyzer.html.parser;

public class HtmlParsingException extends Exception {
	private static final long serialVersionUID = 1L;

	public HtmlParsingException(String msg) {
		super(msg);
	}
}
