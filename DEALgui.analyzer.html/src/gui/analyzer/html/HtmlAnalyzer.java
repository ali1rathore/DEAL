package gui.analyzer.html;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import gui.analyzer.handlers.html.AbstractHtmlHandler;
import gui.analyzer.handlers.html.HtmlHandlers;
import gui.analyzer.html.model.WebPageScene;
import gui.analyzer.html.parser.HtmlGUIParser;
import gui.analyzer.html.parser.HtmlParsingException;
import gui.analyzer.tools.DomainModelGenerator;
import gui.analyzer.tools.exception.ExtractionException;
import gui.model.application.Application;
import gui.model.domain.DomainModel;
import gui.model.domain.Term;

public class HtmlAnalyzer {
	private String url;
	private Application application;
	private DomainModelGenerator generator; 
	
	public HtmlAnalyzer(Application application) {
		this.application = application;
	}
	
	public void analyze(String url) throws HtmlParsingException, ExtractionException {
		this.url = url;
		Document document = parseWebPage();
		WebPageScene wps = generateScene(document.getDocumentElement());

		application.addScene(wps);
	}
	
	private WebPageScene generateScene(Element document) throws ExtractionException {		
		WebPageScene wps = new WebPageScene(document);
		String documentTitle = wps.getName();
		String documentDescription = wps.getDescription();
		
		DomainModel dm = new DomainModel(documentTitle);
		wps.setDomainModel(dm);
		
		AbstractHtmlHandler handler = HtmlHandlers.getInstance().getHtmlHandler(document);
		Term t = handler.createTerm(document, dm);
		
		dm.replaceRoot(t);
		
		HtmlExtractor htmlExtractor = new HtmlExtractor();
		generator = new DomainModelGenerator(htmlExtractor);	
		dm = generator.createDomainModel(wps);
		

		Term root = dm.getRoot();
		if(root != null) {
			root.setName(dm.getName());
			t.setDescription(documentDescription);
		}
		
		return wps;
	}
	
	private Document parseWebPage() throws HtmlParsingException {
		HtmlGUIParser htmlGuiParser = new HtmlGUIParser();
	    Document document = htmlGuiParser.parse(url);
	    return document;
	}

	public String getUrl() {
		return url;
	}
}