# Welcome to the DEAL tool GitLab page!

**DEAL (Domain Extraction ALgorithm)** tool is a domain analysis tool written in Java. DEAL analyses a target application for domain terms and relations and extracts them. Target application is any application created in Java language. The result is a **domain model** which can be used in different processes.

DEAL is:
* open,
* extensible (it is possible to add new handlers for supporting new components).

# DEAL Features

DEAL can:

* create a **platform-independent domain model** from the target application graphical user interface,
* analyze **domain usability** of target appplication based on its domain model,
* generate a **domain-specific language** (grammar, language parser and model represented in Java classes) from the target application graphical user interface,
* generate an **ontology** from the target application graphical user interface,
* generate **new graphical user interfaces** from the generated domain-specific language,
* help you find a term in the graphical user interface.

DEAL can analyze:
* Java applications,
* Windows (*.exe) applications,
* Web applications (under construction).

## How to get and work with DEAL?
Please see the [DEAL Wiki](https://git.kpi.fei.tuke.sk/michaela.bacikova/DEAL/wikis/home) section to start working with DEAL.

The source code of DEAL can be seen in the section [Project/Files](https://git.kpi.fei.tuke.sk/michaela.bacikova/DEAL/tree/master).

##  If you have any ideas, feel free to [CONTACT](https://git.kpi.fei.tuke.sk/michaela.bacikova/DEAL/wikis/home) us! :) 