package gui.analyzer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JOptionPane;

import gui.analyzer.html.HtmlAnalyzer;
import gui.analyzer.html.parser.HtmlParsingException;
import gui.analyzer.tools.exception.ExtractionException;
import gui.analyzer.win.WindowsAnalyzer;
import gui.analyzer.win.parser.WinParsingException;
import gui.editor.DomainModelEditor;
import gui.editor.InputFileDialog;
import gui.model.application.Application;

public class Main {
	private static final String DEAL_UNSUCCESSFUL = "DEAL analysis unsuccessful";
	static HtmlAnalyzer htmlAnalyzer;
	static WindowsAnalyzer winAnalyzer;
	public static boolean ASPECT_ANALYZER = true;
	
	public static void main(String[] args) {
		ASPECT_ANALYZER = false; // run without aspectj

		Application application = new Application();
		DomainModelEditor editor = DomainModelEditor.getInstance();
		editor.setVisible(true);

		editor.setApplication(application);
		
		htmlAnalyzer = new HtmlAnalyzer(application);
		winAnalyzer = new WindowsAnalyzer(application);
		
		boolean success = false;
		while(!success) {
			InputFileDialog dialog = new InputFileDialog();
			dialog.showDialog();
			String url = dialog.getValidatedText();
			
			try {
				tryParse(url);
				success = true;
			} catch (HtmlParsingException | WinParsingException e) {
				JOptionPane.showMessageDialog(editor, "Parsing error, " + e.getMessage(), DEAL_UNSUCCESSFUL,
				        JOptionPane.ERROR_MESSAGE);
			} catch (ExtractionException e) {
				JOptionPane.showMessageDialog(editor, "Extraction error, " + e.getMessage(), DEAL_UNSUCCESSFUL,
				        JOptionPane.ERROR_MESSAGE);
			}
		}

	}
	
	private static void tryParse(String url) throws HtmlParsingException, WinParsingException, ExtractionException {
		if(url != null) {
			if (url.contains("http://") || url.contains("https://")) {
				htmlAnalyzer.analyze(url);
			} else {
				try {
					runMainClass(url.trim());
				} catch (Exception e) {
					e.printStackTrace();
					//if it was not successful, the it is not a main class, but rather a file to analyze with a WindowsAnalyzer
					winAnalyzer.analyze(url);
				}
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked"})
	private static void runMainClass(String className) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
		Class clazz = Class.forName(className);
		Method method = clazz.getMethod("main", String[].class);
		method.invoke(new String[] {});
	}
}
