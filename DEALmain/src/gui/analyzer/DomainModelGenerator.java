package gui.analyzer;

import java.util.List;

import javax.swing.JComponent;

import gui.analyzer.tools.adue.FormStereotypeRecognizer;
import gui.analyzer.tools.adue.ToolTipVerifier;
import gui.analyzer.tools.adue.labelFor.LabelWrapper;
import gui.analyzer.tools.exception.ExtractionException;
import gui.analyzer.tools.extractor.AbstractDealExtractor;
import gui.analyzer.tools.simplifier.Simplifier;
import gui.model.application.scenes.Scene;
import gui.model.domain.DomainModel;
import gui.model.domain.Term;

/**
 * Creates a domain model based on a corresponding Scene. 
 * A Scene can be a window, a dialog, etc.
 * @author Michaela Bacikova, Slovakia,
 * michaela.bacikova@tuke.sk
 * @author Martin Zbu�ka, Slovakia
 */
public class DomainModelGenerator {
	/**
	 * The domain model which is created from the given 
	 * scene is stored in this field - only for the purposes of this class.
	 */
	private DomainModel domainModel;
	private AbstractDealExtractor extractor; // zoberie z rozhrania �daje a vytvor� z toho cel� ten model a �trukt�ru (DomainModel + Term)
	
	private FormStereotypeRecognizer formStereotypeRecognizer;
	private ToolTipVerifier toolTipVerifier;
	
	private Simplifier simplifier; //vyhodi prazdne pojmy, nepotrebne veci z modelu
	
	/**
	 * The constructor creating the new instance of DomainModelGenerator.
	 * Creates a new Extractor and Simplifier.
	 * @param recorder a reference to the recorder
	 */
	public DomainModelGenerator(AbstractDealExtractor extractor) {
		this.extractor = extractor;
		
		this.formStereotypeRecognizer = new FormStereotypeRecognizer();
		this.toolTipVerifier = new ToolTipVerifier();
		
		this.simplifier = new Simplifier();
	}
	
	/**
	 * Creates a domain model from the given scene.
	 * The method has three phases:
	 * <ol>
	 * <li>extraction (using the Extractor instance)</li>
	 * <li>simplification (using the Simplifier instance)</li>
	 * </ol>
	 * @param scene The scene which the model should be created from.
	 * @return The domain model created from the scene.
	 */
	public DomainModel createDomainModel(Scene<?> scene) throws ExtractionException {
		//create a new domain model with a default root and set it to the scene
		createDefaultDomainModel(scene.getName());
		scene.setDomainModel(domainModel);
		
		//1. PHASE, extraction algorithm
		extractor.eXTRACT(scene);
		
		//2. PHASE find form stereotypes and recognize them, evaluate missing tooltips
		formStereotypeRecognizer.recognizeFormStereotypes(domainModel);
		toolTipVerifier.findAndEvaluateComponents(domainModel);
		
		//3. PHASE, simplification algorithm
		simplifier.sIMPLIFY(domainModel);
		
		return domainModel;
	}
	
	private void createDefaultDomainModel(String name) {
		domainModel = new DomainModel(name);
		Term rootTerm = new Term(domainModel, name);
		domainModel.replaceRoot(rootTerm);
	}
	
	public void extractFunctionalComponents(boolean extract) {
		simplifier.extractFunctionalComponents(extract);
	}
	
	public List<JComponent> getToolTipVerifierResults() {
		return toolTipVerifier.getResults();
	}
	
	public List<LabelWrapper> getFormStereotypeRecognizerResults() {
		return formStereotypeRecognizer.getResults();
	}
}