package gui.analyzer.win.parser;

public class WinParsingException extends Exception {
	private static final long serialVersionUID = 1L;

	public WinParsingException(String msg) {
		super(msg);
	}
}
