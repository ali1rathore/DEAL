package gui.analyzer.win;

import gui.analyzer.handlers.win.AbstractWindowsHandler;
import gui.analyzer.handlers.win.WindowsHandlers;
import gui.analyzer.tools.DomainModelGenerator;
import gui.analyzer.tools.exception.ExtractionException;
import gui.analyzer.win.model.WindowsScene;
import gui.analyzer.win.parser.WinParsingException;
import gui.analyzer.win.parser.WindowsGUIParser;
import gui.model.application.Application;
import gui.model.domain.DomainModel;
import gui.model.domain.Term;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WindowsAnalyzer {
	private String filePath;
	private Application application;
	private DomainModelGenerator generator;
	
	public WindowsAnalyzer(Application application) {
		this.application = application;
	}
	
	public void analyze(String filePath) throws ExtractionException, WinParsingException {
		this.filePath = filePath;
		Document document = parseGUI();
		WindowsScene wps = generateScene(document.getDocumentElement());
		application.addScene(wps);
	}
	
	private WindowsScene generateScene(Element document) throws ExtractionException {
		//create new windows scene with the document title and a new domain model
		WindowsScene wps = new WindowsScene(document);
		String documentTitle = wps.getName();
		DomainModel dm = new DomainModel(documentTitle);
		wps.setDomainModel(dm);
		
		//get a handler for the root element to create the root term with the app name
		AbstractWindowsHandler handler = WindowsHandlers.getInstance().getWindowsHandler(document);
		Term t = handler.createTerm(document, dm);

		dm.replaceRoot(t);
		
		//extract information from the document into the domain model
		WindowsExtractor windowsExtractor = new WindowsExtractor();
		generator = new DomainModelGenerator(windowsExtractor);
		dm = generator.createDomainModel(wps);
		
		//set app name to the root element
		Term root = dm.getRoot();
		if(root != null) {
			root.setName(dm.getName());
		}
		
		return wps;
	}
	
	private Document parseGUI() throws WinParsingException {
		WindowsGUIParser winGuiParser = new WindowsGUIParser();
	    Document document = winGuiParser.parse(filePath);
	    return document;
	}

	public String getFilePath() {
		return filePath;
	}
}