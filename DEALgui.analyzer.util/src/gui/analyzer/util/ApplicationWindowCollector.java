package gui.analyzer.util;

import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

public class ApplicationWindowCollector {
	private List<Window> windows = new ArrayList<Window>();
	private static ApplicationWindowCollector instance;

	public List<Window> getApplicationWindows() {
		return windows;
	}
	
	public void addApplicationWindow(Window window) {
		if(!windows.contains(window)) {
			windows.add(window);
		}
	}
	
	public static ApplicationWindowCollector getInstance() {
		if(instance == null) {
			instance = new ApplicationWindowCollector();
		}
		
		return instance;
	}
}
