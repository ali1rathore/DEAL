package gui.analyzer.handlers.win;

import gui.analyzer.handlers.win.impl.WindowsButtonHandler;
import gui.analyzer.handlers.win.impl.WindowsCheckBoxHandler;
import gui.analyzer.handlers.win.impl.WindowsComboBoxHandler;
import gui.analyzer.handlers.win.impl.WindowsContainerHandler;
import gui.analyzer.handlers.win.impl.WindowsLabelHandler;
import gui.analyzer.handlers.win.impl.WindowsListItemHandler;
import gui.analyzer.handlers.win.impl.WindowsMenuBarHandler;
import gui.analyzer.handlers.win.impl.WindowsMenuItemHandler;
import gui.analyzer.handlers.win.impl.WindowsRadioButtonHandler;
import gui.analyzer.handlers.win.impl.WindowsTabPageHandler;
import gui.analyzer.handlers.win.impl.WindowsTableCellHandler;
import gui.analyzer.handlers.win.impl.WindowsTextComponentHandler;
import gui.analyzer.handlers.win.impl.WindowsTextHandler;
import gui.analyzer.handlers.win.impl.WindowsTreeItemHandler;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

public class WindowsHandlers {
	private List<AbstractWindowsHandler> windowsHandlers = new ArrayList<AbstractWindowsHandler>();
	private AbstractWindowsHandler defaultHandler = DefaultWindowsHandler.getInstance();
	
	private static WindowsHandlers instance;
	
	//TODO: Peter ak bude novy handler, tu doplnit
	public WindowsHandlers() {
		windowsHandlers.add(WindowsMenuItemHandler.getInstance());
		windowsHandlers.add(WindowsTextComponentHandler.getInstance());
		windowsHandlers.add(WindowsButtonHandler.getInstance());
		windowsHandlers.add(WindowsComboBoxHandler.getInstance());
		windowsHandlers.add(WindowsContainerHandler.getInstance());
		windowsHandlers.add(WindowsCheckBoxHandler.getInstance());
		windowsHandlers.add(WindowsLabelHandler.getInstance());
		windowsHandlers.add(WindowsMenuBarHandler.getInstance());
		windowsHandlers.add(WindowsRadioButtonHandler.getInstance());
		windowsHandlers.add(WindowsTreeItemHandler.getInstance());
		windowsHandlers.add(WindowsListItemHandler.getInstance());
		windowsHandlers.add(WindowsTableCellHandler.getInstance());
		windowsHandlers.add(WindowsTabPageHandler.getInstance());
		windowsHandlers.add(WindowsTextHandler.getInstance());
		
		//TODO: add new handlers into the htmlHandlers list, otherwise they won't work
		//windowsHandlers.add(...);
		//windowsHandlers.add(...);
		//windowsHandlers.add(...);
		//...
	}
	
	public AbstractWindowsHandler getWindowsHandler(Element element) {
		for (AbstractWindowsHandler windowsHandler : windowsHandlers) {
			if (windowsHandler.matches(element)) {
				return windowsHandler;
			}
		}
		
		return defaultHandler;
	}
	
	/**
	 * Singleton pattern.
	 * @return WindowsHandlers class instance.
	 */
	public static WindowsHandlers getInstance() {
		if (instance == null) {
			instance = new WindowsHandlers();
		}
		return instance;
	}
}
