package gui.analyzer.tools.adue.ui.observable;

import java.util.List;

import javax.swing.JComponent;

/**
 * The event type used for displaying results of ToolTipHelper.
 * 
 * @author Michaela Bacikova, Slovakia,
 * michaela.bacikova@tuke.sk
 */
public class ToolTipHelperEvent {
	private List<JComponent> results;
	
	public ToolTipHelperEvent(List<JComponent> results) {
		this.results = results;
	}
	
	public List<JComponent> getResults() {
		return results;
	}
}
