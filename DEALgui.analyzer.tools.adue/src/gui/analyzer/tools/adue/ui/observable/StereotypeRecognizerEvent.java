package gui.analyzer.tools.adue.ui.observable;

import java.util.List;

import gui.analyzer.tools.adue.labelFor.LabelWrapper;

/**
 * The event type used for displaying results of FormStereotypeRecognizer.
 * 
 * @author Michaela Bacikova, Slovakia,
 * michaela.bacikova@tuke.sk
 */
public class StereotypeRecognizerEvent {
	private List<LabelWrapper> results;
	
	public StereotypeRecognizerEvent(List<LabelWrapper> results) {
		this.results = results;
	}
	
	public List<LabelWrapper> getResults() {
		return results;
	}
}
