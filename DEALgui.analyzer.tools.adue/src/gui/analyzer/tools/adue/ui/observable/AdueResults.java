package gui.analyzer.tools.adue.ui.observable;

import java.util.List;

import javax.swing.JComponent;

import gui.analyzer.tools.adue.labelFor.LabelWrapper;

public class AdueResults extends AdueObservable {
	private List<JComponent> resultsOfToolTipHelper;
	private List<LabelWrapper> resultsOfStereotypeRecognizer;
	
	public void setResultsOfToolTipHelper(List<JComponent> results){
		this.resultsOfToolTipHelper = results;
		notifyResultsOfToolTipHelperChanged();
		/*//zmazat
		int shouldContains = 0;
		int haveToContains = 0;
		for(JComponent c : resultsOfToolTipHelper){
			if (c instanceof AbstractButton) {
				AbstractButton button = (AbstractButton) c;
				if (button.getText() != null) {
					if (!"".equals(button.getText())) {
						shouldContains++;
					} else {
						haveToContains++;
					}
				} else {
				haveToContains++;
				}
			} else {
				// Ak je to iny prvok ako tlacidlo cize custom prvok tak vzdy false.
				haveToContains++;
			}
		}
		System.out.println("Missing ToolTip: " + resultsOfToolTipHelper.size() + "       Have to contains ToolTip: " + haveToContains + "      Should contains Tooltip: " + shouldContains);
		//koniec zmazania
		*/
	}
	
	public void setResultsOfStereotypeRecognizer(List<LabelWrapper> results){
		this.resultsOfStereotypeRecognizer = results;
		notifyResultsOfStereotypeRecognizerChanged();
		/*//zmazat
		int najdenychParov = 0;
		int chybajucichParov = 0;
		for(ComponentsAroundLabel c : resultsOfStereotypeRecognizer){
			if(c.getLabelForComponent() != null){
				najdenychParov++;
			} else {
				chybajucichParov++;
			}
		}
		System.out.println("Without LabelFor: " + resultsOfStereotypeRecognizer.size() + "      Found pairs: " + najdenychParov + "      Missing pairs: " + chybajucichParov);
		//koniec zmazania
	    */
	}
	
	public List<LabelWrapper> getResultsOfStereotypeRecognizer() {
		return resultsOfStereotypeRecognizer;
	}
	
	public List<JComponent> getResultsOfToolTipHelper() {
		return resultsOfToolTipHelper;
	}

	@Override
	public void notifyResultsOfToolTipHelperChanged() {
		setChanged();
		notifyObservers(new ToolTipHelperEvent(resultsOfToolTipHelper));		
	}

	@Override
	public void notifyResultsOfStereotypeRecognizerChanged() {
		setChanged();
		notifyObservers(new StereotypeRecognizerEvent(resultsOfStereotypeRecognizer));		
	}
}
