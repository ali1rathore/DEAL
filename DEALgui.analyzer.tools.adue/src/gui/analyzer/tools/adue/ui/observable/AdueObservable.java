package gui.analyzer.tools.adue.ui.observable;

import java.util.Observable;

/**
 * Observer for the Application class.
 * @author Michaela Bacikova, Slovakia,
 * michaela.bacikova@tuke.sk
 */
public abstract class AdueObservable extends Observable {
	/**
	 * Notifies observers when the tooltip helper sets the results into this object.
	 * @param scene the scene, which was added to the Applications' scene list
	 */
	public abstract void notifyResultsOfToolTipHelperChanged();
	/**
	 * Notifies observers when the stereotype recognizer recognizes a new stereotype and sets the results into this object.
	 * @param scene the scene which was removed
	 */
	public abstract void notifyResultsOfStereotypeRecognizerChanged();
}
