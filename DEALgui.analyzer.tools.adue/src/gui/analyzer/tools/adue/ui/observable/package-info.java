/**
 * Classes for observing the changes caused by the ADUE method analyzer.
 */
package gui.analyzer.tools.adue.ui.observable;
