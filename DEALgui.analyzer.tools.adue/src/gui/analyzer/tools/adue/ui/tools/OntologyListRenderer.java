/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.ui.tools;

import gui.analyzer.tools.adue.evaluationLogic.AbstractEvaluator;
import gui.analyzer.tools.adue.evaluationLogic.OntologyEvaluator;
import gui.analyzer.tools.adue.evaluationLogic.SimpleOntologyEvaluator;
import gui.analyzer.tools.adue.evaluationLogic.results.ElementResultsWrapper;
import gui.analyzer.tools.adue.owl.extracted.AbstractElement;
import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealElement;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Trieda reprezentujuca renderer zoznamu v pouzivatelskom rozhrani. Zabezpecuje
 * vyznacenie prvkov ontologie s chybou. Implementuje od rozhrania
 * ListCellRenderer.
 * 
 * @author Martin Zbuska
 */
@SuppressWarnings("rawtypes")
public class OntologyListRenderer extends JLabel implements ListCellRenderer {
	private static final long serialVersionUID = -1581109950853005865L;
	private static final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);
	private static final Color GRAMMAR_MISTAKE_COLOR = Color.RED; //249, 142, 164, 255
	private static final Color PARENT_MISTAKE_COLOR = new Color(249, 190, 103, 255);
	private static final Color ADVERSE_CHANGE_COLOR = new Color(249, 142, 164, 255); //222, 101, 185, 255

	private final String TOOLTIP_WRONG_GRAMMAR = "Contains grammatical mistakes.";
	private final String TOOLTIP_WRONG_PARENT = "Contains wrong defined parent name.";
	private final String TOOLTIP_WRONG_CHANGE = "Represents adverese component change.";

	/**
	 * Objekt reprezentujuci prvy sposob vykonanej extrakcie/hodnotenia ontologie.
	 */
	private OntologyEvaluator ontologyEvaluator;
	
	/**
	 * Objekt reprezentujuci druhy sposob vykonanej extrakcie/hodnotenia ontologie.
	 */
	private SimpleOntologyEvaluator simpleOntologyEvaluator;

	/**
	 * Konstruktor.
	 * 
	 * @param evaluator
	 *            objekt ktory vykonaval hodnotenie ontologii, a obsahuje
	 *            vysledky hodnotenia.
	 */
	public OntologyListRenderer(AbstractEvaluator evaluator) {
		setOpaque(true);
		setIconTextGap(12);
		if (evaluator instanceof OntologyEvaluator) {
			this.ontologyEvaluator = (OntologyEvaluator) evaluator;
		} else {
			this.simpleOntologyEvaluator = (SimpleOntologyEvaluator) evaluator;
		}
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		String text = ((AbstractElement) value).getId();
		for (int i = text.length(); i < 50 - text.length(); i++) {
			text += " ";
		}
		text += ((AbstractElement) value).getComponentClass();
		setText(text);
		setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

		if (value instanceof DealComponent) {
			DealComponent component = (DealComponent) value;

			setBackground(Color.WHITE);
			setForeground(Color.BLACK);
			setToolTipText(null);
			if (ontologyEvaluator != null) {
				if (ontologyEvaluator.getOneOntologyResults().getListOfGrammaticalErrors().contains(new ElementResultsWrapper(component, null, null))) {
					setBackground(GRAMMAR_MISTAKE_COLOR);
					setToolTipText(TOOLTIP_WRONG_GRAMMAR);
				}
				if (ontologyEvaluator.getOneOntologyResults().getListOfWrongParents().contains(new ElementResultsWrapper(component, null, null))) {
					setBackground(PARENT_MISTAKE_COLOR);
					String tooltip = getToolTipText();
					if (tooltip != null) {
						setToolTipText(tooltip + " & " + TOOLTIP_WRONG_PARENT);
					} else {
						setToolTipText(TOOLTIP_WRONG_PARENT);
					}
				}
				if (ontologyEvaluator.getTwoOntologiesResults() != null) {
					if (ontologyEvaluator.getTwoOntologiesResults().getAdverseChangedComponent1ByComponent2(component) != null) {
						setBackground(ADVERSE_CHANGE_COLOR);
						String tooltip = getToolTipText();
						if (tooltip != null) {
							setToolTipText(tooltip + " & " + TOOLTIP_WRONG_CHANGE);
						} else {
							setToolTipText(TOOLTIP_WRONG_CHANGE);
						}
					}
				}
			}
			if (isSelected) {
				setBackground(HIGHLIGHT_COLOR);
				setForeground(Color.WHITE);
			}

		} else if (value instanceof DealElement) {
			DealElement element = (DealElement) value;

			setBackground(Color.WHITE);
			setForeground(Color.BLACK);
			setToolTipText(null);
			if (simpleOntologyEvaluator != null) {
				if (simpleOntologyEvaluator.getSimpleOneOntologyResults().getListOfGrammaticalErrors().contains(new ElementResultsWrapper(element, null, null))) {
					setBackground(GRAMMAR_MISTAKE_COLOR);
					setToolTipText(TOOLTIP_WRONG_GRAMMAR);
				}
				if (simpleOntologyEvaluator.getSimpleOneOntologyResults().getListOfWrongParents().contains(new ElementResultsWrapper(element, null, null))) {
					setBackground(PARENT_MISTAKE_COLOR);
					String tooltip = getToolTipText();
					if (tooltip != null) {
						setToolTipText(tooltip + " & " + TOOLTIP_WRONG_PARENT);
					} else {
						setToolTipText(TOOLTIP_WRONG_PARENT);
					}
				}
			}
			if (isSelected) {
				setBackground(HIGHLIGHT_COLOR);
				setForeground(Color.WHITE);
			}
		}

		if (ontologyEvaluator == null && simpleOntologyEvaluator == null) {
			if (isSelected) {
				setBackground(HIGHLIGHT_COLOR);
				setForeground(Color.WHITE);
			} else {
				setBackground(Color.WHITE);
				setForeground(Color.BLACK);
			}
		}

		return this;
	}
}
