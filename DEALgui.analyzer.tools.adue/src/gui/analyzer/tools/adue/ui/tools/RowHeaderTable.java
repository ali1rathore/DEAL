/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.ui.tools;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 * Trieda reprezentujuca hlavicku riadkvo v tabulke pouzivatelskeho rozhrania.
 * Dedi od triedy JTable.
 * 
 * @author Martin Zbuska
 */
public class RowHeaderTable extends JTable {
	private static final long serialVersionUID = 5418268720979414820L;
	
	/**
	 * Nazvy hlaviciek riadkov.
	 */
	private final String[] rowNames;

	/**
	 * Konstruktor.
	 * 
	 * @param rowNames
	 *            zoznam nazvov riadkov tabulky.
	 */
	public RowHeaderTable(String[] rowNames) {
		this.rowNames = rowNames;
		setModel(getHeaderModel());
		populateHeader();
		setShowGrid(false);
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setPreferredScrollableViewportSize(new Dimension(90, 0));
		getColumnModel().getColumn(0).setPreferredWidth(90);
		setRowHeight(20);
		getColumnModel().getColumn(0).setCellRenderer(new TableRowHeaderRenderer());
	}

	private void populateHeader() {
		for (int i = 0; i < rowNames.length; i++) {
			this.setValueAt(rowNames[i], i, 0);
		}
	}

	private DefaultTableModel getHeaderModel() {
		DefaultTableModel headerModel = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public int getColumnCount() {
				return 1;
			}

			@Override
			public boolean isCellEditable(int row, int col) {
				return false;
			}

			@Override
			public int getRowCount() {
				return rowNames.length;
			}

			@Override
			public Class<?> getColumnClass(int colNum) {
				switch (colNum) {
				case 0:
					return String.class;
				default:
					return super.getColumnClass(colNum);
				}
			}
		};
		return headerModel;
	}

	class TableRowHeaderRenderer implements TableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			table.setCellSelectionEnabled(false);
			table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setEnabled(false);
			Component component = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, false, false, -1, -2);
			((JLabel) component).setHorizontalAlignment(SwingConstants.CENTER);

			return component;
		}

	}

}
