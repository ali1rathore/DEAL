/**
 * Tento balik zahrna triedy zabezpecujuce vykreslovanie konkretnych veci podla poziadavok.
 * 
 * Trieda OntologyListRenderer zabezpecuje vyznacovanie poloziek v zoznamoch extrahovanych poloziek z ontologii.
 * 
 * Trieda RowHeaderTable zabezpecuje vykreslenie hlaviciek riadkov tabulky v GUI nastroja.
 */
package gui.analyzer.tools.adue.ui.tools;