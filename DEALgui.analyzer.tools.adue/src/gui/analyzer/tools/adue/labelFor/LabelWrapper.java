package gui.analyzer.tools.adue.labelFor;

import java.awt.Component;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;

/**
 * Holds the tested JLabel component and all components
 * that are the closest around this component, 
 * as well its labelFor component.
 * 
 * @author Martin Zbu�ka
 * 
 */
public class LabelWrapper {

	/**
	 * JLabel for which we try to find components suitable for its labelFor attribute.
	 */
	private JLabel label;
	
	/**
	 * Closest components to the <code>label</code> in particular directions of <code>Direction</code> enum.
	 */
	private Map<Direction, Component> components;

	/**
	 * The resulting component, which was determined as suitable for the tested <code>JLabel</code>'s 
	 * <code>labelFor</code> attribute.
	 */
	private Component labelForComponent;

	/**
	 * Constructor.
	 * 
	 * @param label the JLabel component, for which we search for the labelFor component.
	 */
	public LabelWrapper(JLabel label) {
		this.label = label;
		//if there is a label for, set it, otherwise it will be null
		this.labelForComponent = label.getLabelFor();
		this.components = new HashMap<>(); 
	}

	/**
	 * @return the direction in which the assigned component for the label is.
	 * If there is no assigned component, returns <code>Direction.MIX</code>.
	 */
	public Direction getStereotypeLabelForDirection() {
		for(Direction d : components.keySet()) {
			if(labelForComponent == components.get(d)) {
				return d;
			}
		}
		return Direction.MIX;
	}

	@Override
	public String toString() {
		@SuppressWarnings("resource")
		Formatter f = new Formatter();
		
		f.format("%s :    %s\n", label.getClass().getSimpleName(), label.getText());
		
		for(Direction d : Direction.values()) {
			Component c = components.get(d);
			f.format("%s: %s\n", d.getName(), components.get(d) == null ? "null" : c.getClass().getSimpleName());
		}
		
		f.format("Component labelFor component: %s\n", labelForComponent != null ? labelForComponent.getClass().getSimpleName() : "null");
		
		return f.toString();
	}
	
	/**
	 * @param direction the direction for which the component is to be returned.
	 * @return The component in the given direction. If the direction is <code>Direction.MIX</code>, 
	 * or when there is no component for that direction, returns null.
	 */
	public Component getComponent(Direction direction) {
		return components.get(direction);
//		switch(direction) {
//			case UP: return componentUp;
//			case DOWN: return componentDown;
//			case RIGHT: return componentRight;
//			case LEFT: return componentLeft;
//			default: return null;
//		}
	}
	
	/**
	 * Stores the component to the designated direction. If the direction is <code>Direction.MIX</code>, 
	 * then the component is not stored anywhere.
	 * @param c the component which is to be stored in the designated direction
	 * @param d the direction, to which the component is to be stored
	 */
	public void setComponent(Component c, Direction d) {
		if(d == Direction.MIX) return;
		components.put(d, c);
//		switch(d) {
//			case UP: this.componentUp = c; break;
//			case DOWN: this.componentDown = c; break;
//			case RIGHT: this.componentRight = c; break;
//			case LEFT: this.componentLeft = c; break;
//			default: return;
//		}
	}

	/**
	 * @return the labelForComponent
	 */
	public Component getLabelForComponent() {
		return labelForComponent;
	}

	/**
	 * @param labelForComonent
	 *            the labelForComponent to set
	 */
	public void setLabelForComponent(Component labelForComonent) {
		this.labelForComponent = labelForComonent;
	}

	/**
	 * @return the label
	 */
	public JLabel getLabel() {
		return label;
	}

}
