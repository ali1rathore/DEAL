package gui.analyzer.tools.adue.labelFor;

/**
 * Enumeracny typ, reprezentujuci smery, v ktorych sa bude hladat komponenty voci komponentu JLabel.
 * @author Martin Zbuska
 *
 */
public enum Direction {

	RIGHT("Right"), LEFT("Left"), UP("Up"), DOWN("Down"), MIX("Mix");
	
	private String name;
	
	Direction(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
}
