package gui.analyzer.tools.adue.labelFor;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Trieda zabezpecujuca vyznacenie objektov v strome komponentov, ktore nemaju
 * nastaveny atribut toolTip. Dedi od triedy DefaultTreeCellRenderer.
 * 
 * @author Martin Zbuska
 * 
 */
public class ToolTipTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 1L;

	private static final Color BG_COLOR_NORMAL = Color.WHITE;

	private static final Color BG_COLOR_MISSING_TOOLTIP = Color.RED;
	private static final Color BG_COLOR_SHOULD_CONTAIN_TOOLTIP = Color.ORANGE;
	
	private static final String RECOMMENDATION_MESSAGE = "<html><b>Recommendation:</b><br>Should contain ToolTip.</html>";
	private static final String USABILITY_ISSUE_MESSAGE = "<html><b>Usability issue:</b><br>Have to set ToolTip!</html>";

	/**
	 * Zoznam komponentov bez nastaveneho atributu tooltip.
	 */
	private List<JComponent> resultsOfToolTipHelper;

	/**
	 * Konstruktor.
	 * 
	 * @param results
	 *            zoznam komponentov ktore nemaju nastaveny atribut toolTip.
	 */
	public ToolTipTreeCellRenderer(List<JComponent> results) {
		this.resultsOfToolTipHelper = results;
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

		if (node.getUserObject() instanceof JComponent) {
			JComponent component = (JComponent) node.getUserObject();
			if (getText() == null || "".equals(getText())) {
				setText("  ");
			}
			if (this.resultsOfToolTipHelper.contains(component)) {
				setToolTipText(isContainingText(component) ? RECOMMENDATION_MESSAGE : USABILITY_ISSUE_MESSAGE);
				Color bgColor = isContainingText(component) ? BG_COLOR_SHOULD_CONTAIN_TOOLTIP
				        : BG_COLOR_MISSING_TOOLTIP;
				setBackgroundNonSelectionColor(bgColor);
			} else {
				setToNormal();
			}

		} else {
			setToNormal();
		}

		return this;
	}

	/**
	 * Nastavi normalnu farbu polozky v strome.
	 */
	private void setToNormal() {
		setToolTipText(null);
		Color bgColor = BG_COLOR_NORMAL;
		setBackgroundNonSelectionColor(bgColor);
	}

	/**
	 * Overuje ci sa jedna o polozku tlacidla a ci obsahuje text, na zaklade
	 * toho sa zvoli farba polozky v strome.
	 * 
	 * @param component
	 *            objekt vykreslovaneho komponentu.
	 * @return true ak je dany objekt tlacidlom a nema nastaveny text, inac
	 *         false.
	 */
	private boolean isContainingText(JComponent component) {
		if (component instanceof AbstractButton) {
			AbstractButton button = (AbstractButton) component;
			if (button.getText() != null) {
				if (!"".equals(button.getText())) {
					return true;
				}
			}
			return false;
		} else {
			// Ak je to iny prvok ako tlacidlo cize custom prvok tak vzdy false.
			return false;
		}
	}

}
