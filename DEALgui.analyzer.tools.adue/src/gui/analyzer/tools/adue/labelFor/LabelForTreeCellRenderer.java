package gui.analyzer.tools.adue.labelFor;

import java.awt.Color;
import java.awt.Component;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Trieda zabezpecujuca vyznacenie objektov JLabel v strome komponentov, ktore
 * nemaju nastaveny atribut labelFor. Dedi od triedy DefaultTreeCellRenderer.
 * 
 * @author Martin Zbuska
 * 
 */
public class LabelForTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 1L;

	private static final Color BG_COLOR_NORMAL = Color.WHITE;

	private static final Color BG_COLOR_MISSING_LABELFOR = Color.RED;
	private static final Color BG_COLOR_SET_LABELFOR_TO = Color.ORANGE;

	private String RECOMMENDATION_MESSAGE;
	private static final String USABILITY_ISSUE_MESSAGE = "<html><b>Usability issue:</b><br>Cannot find component to labelFor attribute for this Label!</html>";

	/**
	 * Zoznam JLabelov a im piradenych komponentov.
	 */
	private List<LabelWrapper> resultsOfStereotypeRecognizer;

	/**
	 * Konstruktor.
	 * 
	 * @param results
	 *            zoznam labelov ktore nemaju nastaveny atribut labelFor.
	 */
	public LabelForTreeCellRenderer(List<LabelWrapper> results) {
		this.resultsOfStereotypeRecognizer = results;
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

		// Ak je dany komponent label tak pokracuj.
		if (node.getUserObject() instanceof JLabel) {

			JLabel component = (JLabel) node.getUserObject();
			// Ak dany label nema nastaveny atribut LabelFor, tak najdi vhodny
			// komponent vo vysledkoch
			if (component.getLabelFor() == null) {

				LabelWrapper findedMatch = null;

				for (LabelWrapper result : resultsOfStereotypeRecognizer) {
					if (result.getLabel().equals(component)) {
						findedMatch = result;
						break;
					}
				}

				// Ak sa vo vysledkoch nasiel label ktory zodpoveda aktualne
				// nastavovanemu labelu
				// tak mu nastav farbu a tooltip ze na aky komponent sa ma
				// nastavit labelFor, ak vo vysledkoch nejaky je.
				if (findedMatch != null) {
					if (getText() == null || "".equals(getText())) {
						setText("  ");
					}
					if (findedMatch.getLabelForComponent() != null) {
						RECOMMENDATION_MESSAGE = "<html><b>Recommendation:</b><br>"
								+ "Set the labelFor attribute of  \"" + findedMatch.getLabel().getText() + "\" label<br>to refer to the " 
								+ findedMatch.getLabelForComponent().getClass().getSimpleName()
								+ " located<br>on the " + findedMatch.getStereotypeLabelForDirection()
								+ " side of  \"" + findedMatch.getLabel().getText() + "\"."
								+ "</html>";
						setToolTipText(RECOMMENDATION_MESSAGE);
						Color bgColor = BG_COLOR_SET_LABELFOR_TO;
						setBackgroundNonSelectionColor(bgColor);
					} else {
						setToolTipText(USABILITY_ISSUE_MESSAGE);
						Color bgColor = BG_COLOR_MISSING_LABELFOR;
						setBackgroundNonSelectionColor(bgColor);
					}
				} else {
					setToNormal();
				}

			} else {
				setToNormal();
			}

		} else {
			setToNormal();
		}

		return this;
	}

	/**
	 * Nastavi normalnu farbu polozky v strome.
	 */
	private void setToNormal() {
		setToolTipText(null);
		Color bgColor = BG_COLOR_NORMAL;
		setBackgroundNonSelectionColor(bgColor);
	}
}
