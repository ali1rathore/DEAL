package gui.analyzer.tools.adue.labelFor;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * Holds a list of components and list of labels of domain model
 * with the same parent component (container).
 * 
 * @author Martin Zbu�ka, Michaela Ba��kov�
 *
 */
public class ContainerWrapper {

	/**
	 * List of JLabels with the same parent component (the same parent as componentsOfCommonParent).
	 */
	private List<LabelWrapper> labelsOfCommonParent;

	/**
	 * List of components with the same parent component (the same parent as labelsOfCommonParent).
	 */
	private List<Component> componentsOfCommonParent;

	public ContainerWrapper() {
		this.labelsOfCommonParent = new ArrayList<LabelWrapper>();
		this.componentsOfCommonParent = new ArrayList<Component>();
	}

	/**
	 * @return the listOfLabels
	 */
	public List<LabelWrapper> getLabels() {
		return labelsOfCommonParent;
	}

	/**
	 * @return the listOfComponents
	 */
	public List<Component> getComponents() {
		return componentsOfCommonParent;
	}

}
