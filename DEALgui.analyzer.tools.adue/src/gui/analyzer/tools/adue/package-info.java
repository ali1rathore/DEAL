/**
 * Contains classes of the adue tool analyzer for evaluating domain usability by
 * comparing and evaluating ontologies.
 *
 * <ul>
 * <li>
 * package <code>evaluationLogic</code> includes the classes representing
 * the logical part of comparing and evaluating ontologies.
 * </li>
 * <li>
 * package <code>main</code> contains the main class of the tool for
 * purposes of stand-alone run.
 * </li>
 * <li>
 * package <code>owl</code> contains classes for processing ontologies and
 * corresponding outputs.
 * </li>
 * <li>
 * package <code>ui</code> contains files *.png and subpackage
 * <code>tools</code> with classes ensuring
 * the display of the evaluation data in the GUI.
 * <li>
 * </ul>
 */
package gui.analyzer.tools.adue;