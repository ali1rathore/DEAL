package gui.analyzer.tools.adue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComponent;

import gui.analyzer.util.Logger;
import gui.model.domain.ComponentInfoType;
import gui.model.domain.DomainModel;
import gui.model.domain.Term;
import gui.model.domain.Util;

/**
 * Verifies functional and custom components of the tested application UI, which
 * have no tool-tip attribute set.
 * If a component has no name nor a description, it has to have a tool-tip set.
 * @author Martin Zbuska
 */
public class ToolTipVerifier {

	/**
	 * List of components that need to have a tooltip attribute set.
	 */
	public List<JComponent> results;

	/**
	 * Constructor, initializes the results list.
	 */
	public ToolTipVerifier() {
		results = new ArrayList<JComponent>();
	}

	/**
	 * Performs all necessary steps to evaluate tooltips in the domain model
	 * 
	 * @param domainModel the domain model to evaluate
	 */
	public void findAndEvaluateComponents(DomainModel domainModel) {
		clearResults();
		getFunctionalComponentsFromDomainModel(domainModel.getAllTerms());
	}

	public List<JComponent> getResults() {
		return this.results;
	}


	/**
	 * Erase the list of results.
	 */
	private void clearResults() {
		this.results.clear();
	}

	/**
	 * Finds and adds to a list of results the functional or custom components
	 * that have no usable identifier (name) and no tool-tip.
	 * DEAL then lets the user know that s/he needs to add a tool-tip to those
	 * components.
	 * 
	 * @param terms a list of terms extracted from the tested application UI.
	 */
	private void getFunctionalComponentsFromDomainModel(List<Term> terms) {
		Iterator<Term> iter = terms.iterator();
		while (iter.hasNext()) {
			Term t = iter.next();
			if (t.getComponentInfoType() == ComponentInfoType.FUNCTIONAL
			        || t.getComponentInfoType() == ComponentInfoType.CUSTOM) {
				Object c = t.getComponent();

				if (c == null) {
					Logger.logError("Term   ->  ' " + t +
					        " ' is not a component, it's of type ->    " + t.getComponentClass());
					continue;
				}

				if (needsTooltip(t)) {
					results.add((JComponent) c);
				}
			}
		}
	}

	/**
	 * @param t the term to be checked for tooltip
	 * @return true if the representing component does not have a tooltip
	 *         (description). Also true if the component
	 *         is functional and has no name. False otherwise.
	 */
	private boolean needsTooltip(Term t) {
		return Util.isEmpty(t.getDescription()) &&
		        !(t.getComponentInfoType() == ComponentInfoType.FUNCTIONAL
		                && !Util.isEmpty(t.getName()));
	}
}
