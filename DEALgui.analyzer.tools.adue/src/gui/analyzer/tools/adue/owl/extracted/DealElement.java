/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.owl.extracted;

import java.util.ArrayList;

/**
 * Trieda reprezentujuca extrahovany DEALElement cize polozka z ontologie. Dedi
 * od abstraktnej triedy AbstractElement, ktora obsahuje atributy ktore maju
 * triedy DealComponent a DealElement po extrakcii z ontologie rovnake.
 * 
 * @author Martin Zbuska
 */
public class DealElement extends AbstractElement {

	/**
	 * Description extrahovaneho elementu z ontologie.
	 */
	private String description;
	
	/**
	 * Meno extrahovaneho elementu z ontologie.
	 */
	private String name;
	
	/**
	 * Label extrahovaneho elementu z ontologie.
	 */
	private String label;

	/**
	 * Konstruktor.
	 */
	public DealElement() {
		this.childs = new ArrayList<>();
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Kvoli volaniu metody contains nad zoznamom.
	 * 
	 * @param obj
	 * @return true ak sa objekty rovnaju inak false
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof DealElement) {
			DealElement element = (DealElement) obj;
			return (element.id == null ? this.id == null : element.id.equals(this.id)) && (element.componentClass == null ? this.componentClass == null : element.componentClass.equals(this.componentClass)) && (element.name == null ? this.name == null : element.name.equals(this.name)) && (element.label == null ? this.label == null : element.label.equals(this.label)) && (element.description == null ? this.description == null : element.description.equals(this.description))
					&& (element.parent == null ? this.parent == null : element.parent.equals(this.parent)) && (element.getChilds().toString() == null ? this.childs.toString() == null : element.getChilds().toString().equals(this.childs.toString()));
		}
		return false;
	}

	/**
	 * @return zoznam nazvov atributov danej triedy.
	 */
	public static String[] getPropertyNames() {
		String[] propertyNames = new String[] { "ID", "Name", "Class", "Label", "Description", "Parent", "Childs" };
		return propertyNames;
	}
}
