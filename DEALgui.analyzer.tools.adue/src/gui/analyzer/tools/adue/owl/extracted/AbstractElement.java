/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.owl.extracted;

import java.util.List;

/**
 * Abstraktna trieda, reprezentujuca spolocne prvky extrahovanych poloziek z
 * ontologie (sposobom ziskavania komponentov -DealComponent alebo elementov
 * -DealElement).
 * 
 * @author Martin Zbuska
 */
public abstract class AbstractElement {

	/**
	 * ID extrahovanej polozky.
	 */
	protected String id;
	
	/**
	 * Typ extrahovanej polozky.
	 */
	protected String componentClass;
	
	/**
	 * Rodicovsky pojem extrahovanej polozky.
	 */
	protected String parent;
	
	/**
	 * Potomkovia extrahovanej polozky.
	 */
	protected List<String> childs;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the componentClass
	 */
	public String getComponentClass() {
		return componentClass;
	}

	/**
	 * @param componentClass
	 *            the componentClass to set
	 */
	public void setComponentClass(String componentClass) {
		this.componentClass = componentClass;
	}

	/**
	 * @return the parent
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * @return the childs
	 */
	public List<String> getChilds() {
		return childs;
	}

	/**
	 * @param childs
	 *            the childs to set
	 */
	public void setChilds(List<String> childs) {
		this.childs = childs;
	}

}
