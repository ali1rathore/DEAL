/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.owl.extracted;

/**
 * Trieda reprezentujuca par dvoch KOMPONENTOV extrahovanych z ontologie. Pouziva sa pri extrahovani komponentov z ontologie, vzhladom na to ze polozky v ontologii maju za urcitych podmienok zlucene komponenty JLabel a napr JTextField. Zaroven sa pouziva pri uchovavani vysledkov hodnotenia pri zmenenych prvkoch ako povodny a novy komponent.
 * @author Martin Zbuska
 */
public class DealComponentPair {
    
	/**
	 * Extrahovany komponent z ontologie. (zo starej)
	 */
    private final DealComponent component1;
    
    /**
     * Extrahovany komponent z ontologie. (z novej)
     */
    private DealComponent component2;
    
    /**
     * Konstruktor.
     * @param component
     */
    public DealComponentPair(DealComponent component){
        this.component1 = component;
    }
    
    /**
     * Konstruktor.
     * @param component1
     * @param component2
     */
    public DealComponentPair(DealComponent component1, DealComponent component2){
        this.component1 = component1;
        this.component2 = component2;
    }

    /**
     * @return the component1
     */
    public DealComponent getComponent1() {
        return component1;
    }

    /**
     * @return the component2
     */
    public DealComponent getComponent2() {
        return component2;
    }
    
    
    @Override
    public boolean equals(Object obj){
        if(obj != null && obj instanceof DealComponentPair){
            DealComponentPair component = (DealComponentPair)obj;
            return (component.getComponent1().equals(component1) && component.getComponent2().equals(component2));
        }
        return false;
    }
    
}
