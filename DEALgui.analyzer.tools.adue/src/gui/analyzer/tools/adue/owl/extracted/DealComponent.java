/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.owl.extracted;

import java.util.List;

/**
 * Trieda reprezentujuca extrahovany KOMPONENT z ontologie. Dedi od abstraktnej
 * triedy AbstractElement, ktora obsahuje atributy ktore maju triedy
 * DealComponent a DealElement po extrakcii z ontologie rovnake.
 * 
 * @author Martin Zbuska
 */
public class DealComponent extends AbstractElement {

	/**
	 * Text, ktory obsahuje extrahovany komponent.
	 */
	private final String text;

	/**
	 * Konstruktor.
	 * 
	 * @param id
	 *            komponentu
	 * @param componentClass
	 *            typ komponentu
	 * @param name
	 *            meno/text komponentu
	 * @param parent
	 *            rodicovsky pojem
	 * @param childs
	 *            zoznam dcerskych pojmov
	 */
	public DealComponent(String id, String componentClass, String name, String parent, List<String> childs) {
		this.id = id;
		this.componentClass = componentClass;
		this.text = name;
		this.parent = parent;
		this.childs = childs;
	}

	/**
	 * Metoda overujuca ci sa objekt tejto triedy nachadza v zozname, ktory je
	 * dany ako parameter. Objekt sa v zozname nachadza ak nejaky objekt zo
	 * zoznamu ma rovnaky atribut typu komponentu a rovnaky name/text.
	 * 
	 * @param list
	 *            zoznam komponentov v ktorom sa objekt tejto triedy hlada.
	 * @return polozka zo zonamu ktora je podla podmienky zhodna s tymto
	 *         objektom. Ak sa v zozname nenachadza tak null.
	 */
	public DealComponent isContainedInList(List<DealComponent> list) {
		for (DealComponent componentInList : list) {
			if (componentInList.getComponentClass().equals(this.componentClass) && componentInList.getText().equals(this.text)) {
				return componentInList;
			}
		}
		return null;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	@Override
	public String toString() {
		String string = "\nID: " + this.id + "\n     componentClass: " + this.componentClass + "\n     text: " + this.text + "\n     parent: " + this.parent + "\n     childs: " + this.childs;
		return string;
	}

	/**
	 * Kvoli volaniu metody contains nad zoznamom.
	 * 
	 * @param obj
	 * @return true ak sa objekty rovnaju, inak false
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof DealComponent) {
			DealComponent component = (DealComponent) obj;
			return (component.id == null ? this.id == null : component.id.equals(this.id)) && (component.componentClass == null ? this.componentClass == null : component.componentClass.equals(this.componentClass)) && (component.text == null ? this.text == null : component.text.equals(this.text)) && (component.parent == null ? this.parent == null : component.parent.equals(this.parent))
					&& (component.getChilds().toString() == null ? this.childs.toString() == null : component.getChilds().toString().equals(this.childs.toString()));
		}
		return false;
	}

	/**
	 * @return zoznam nazvov atributov danej triedy.
	 */
	public static String[] getPropertyNames() {
		String[] propertyNames = new String[] { "ID", "Class", "Text", "Parent", "Childs" };
		return propertyNames;
	}
}
