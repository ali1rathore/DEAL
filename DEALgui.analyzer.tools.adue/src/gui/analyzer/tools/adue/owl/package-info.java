/**
 * Tento balik zahrna triedu OWLParser sluziacu na parsovanie ontologii za ucelom ich hodnotenia. Zaroven obsahuje balik extracted s triedamy reprezentujucimi
 * extrahovane objekty z ontologii.
 */
package gui.analyzer.tools.adue.owl;