/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.owl;

import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealComponentPair;
import gui.analyzer.tools.adue.owl.extracted.DealElement;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 * Trieda, ktora zabezpecuje extrahovanie prvkov z ontologie. Vyuziva kniznicu
 * OWLAPI.
 * 
 * @author Martin Zbuska
 */
public class OWLParser {

	/**
	 * Konstruktor.
	 */
	private OWLParser() {
	}

	/**
	 * Kedze sa jedna o SINGLETON.
	 * 
	 * @return objekt tejto triedy.
	 */
	public static OWLParser getInstance() {
		return OWLParserHolder.INSTANCE;
	}

	private static class OWLParserHolder {
		private static final OWLParser INSTANCE = new OWLParser();
	}

	/**
	 * Vytvori a vrati zoznam komponentov a labelov z otologie.
	 * 
	 * @param path
	 *            cesta k suboru ontologie ../*.owl
	 * @return zoznam komponentov a labelov extrahovanych z ontologie.
	 * @throws OWLOntologyCreationException
	 */
	public List<DealComponent> getListOfComponentsFromOntology(String path) throws OWLOntologyCreationException {
		File baseFile = new File(path);
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(baseFile);

		List<DealComponent> listOfComponents = new ArrayList<>();

		for (OWLClass owlClass : ontology.getClassesInSignature()) {
			DealElement element = getDealElement(owlClass, ontology);
			// Ak bolo id, name, alebo componentClass nedefinovane tak element
			// je null, takze ho nespracovavat!
			if (element != null) {
				// Z OwlElement ziskat komponent, a ak tam je tak aj label
				DealComponentPair componentPair = getDealComponentPair(element);
				// Pridat ziskane vysledky do listu
				if (componentPair != null) {
					// pridanie komponentu do zoznamu
					if (componentPair.getComponent1() != null && componentPair.getComponent1().getComponentClass() != null) {
						listOfComponents.add(componentPair.getComponent1());
					}
					// pridanie labelu do zoznamu
					if (componentPair.getComponent2() != null && componentPair.getComponent2().getComponentClass() != null) {
						listOfComponents.add(componentPair.getComponent2());
					}
				}
			}
		}
		/**
		 * odstrania sa komponenty ktore kopiruju parenta ako napr: ID: Gender
		 * ID: man componentClass: JLabel componentClass: JLabel text: Gender:
		 * text: Gender: parent: Person parent: Gender childs: [man, woman]
		 * childs: []
		 */
		deleteFalseComponents(listOfComponents);
		return listOfComponents;
	}

	/**
	 * Vytvori a vrati zoznam elementov obsiahnutych v ontologii pre jednoduche
	 * porovnanie dvoch ontologii. Tieto elementy su extrahovane mnou vytvorene
	 * DealElementy.
	 * 
	 * @param path
	 *            cesta k suboru ontologie ../*.owl
	 * @return zoznam elementov extrahovanych z ontologie.
	 * @throws OWLOntologyCreationException
	 */
	public List<DealElement> getListOfOntologyElements(String path) throws OWLOntologyCreationException {
		File baseFile = new File(path);
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(baseFile);

		List<DealElement> listOfElements = new ArrayList<>();
		for (OWLClass owlClass : ontology.getClassesInSignature()) {
			DealElement element = getDealElement(owlClass, ontology);
			listOfElements.add(element);
		}
		return listOfElements;
	}

	/**
	 * Vytvori objekt typu DealElement. Tento objekt predstavuje jednu polozku z
	 * ontologie.
	 * 
	 * @param owlClass
	 *            polozka z ontologie na spracovanie
	 * @param ontology
	 *            ontologia z ktorej sa extrahuje
	 * @return ak je id, name, alebo componentClass v ontologii nedefinovane tak
	 *         null, inac element
	 */
	private DealElement getDealElement(OWLClass owlClass, OWLOntology ontology) {
		DealElement element = new DealElement();

		// set id of element
		element.setId(owlClass.getIRI().getFragment());

		// set parent if exist
		Set<OWLClassExpression> superClasses = owlClass.getSuperClasses(ontology);
		String parent = "";
		if (!superClasses.isEmpty()) {
			parent = superClasses.iterator().next().asOWLClass().getIRI().getFragment();
		}
		element.setParent(parent);

		// set childs if exist
		Set<OWLClassExpression> subClasses = owlClass.getSubClasses(ontology);
		List<String> childs = new ArrayList<>();
		String child;
		for (OWLClassExpression subClass : subClasses) {
			child = subClass.asOWLClass().getIRI().getFragment();
			childs.add(child);
		}
		element.setChilds(childs);

		// set other attributes == componentClass, name, label, description
		for (OWLClassExpression expression : owlClass.getEquivalentClasses(ontology)) {
			try {
				OWLDataHasValue dataHasValue = (OWLDataHasValue) expression;
				OWLLiteral literal = dataHasValue.getValue();

				String property = dataHasValue.getProperty().asOWLDataProperty().getIRI().getFragment();
				String value = literal.getLiteral();
				switch (property) {
				case "label":
					element.setLabel(value);
					break;
				case "componentClass":
					element.setComponentClass(value);
					break;
				case "name":
					element.setName(value);
					break;
				case "description":
					element.setDescription(value);
					break;
				}
			} catch (Exception ex) {
				// ex.printStackTrace();
			}
		}
		return element;
	}

	/**
	 * Ziska z objektu DealElement komponent s textom, a ak pre dany komponent
	 * je labelFor tak aj label. Ak je dany komponent label tak vrati ten ako
	 * komponent a labelFor nastavi na null.
	 * 
	 * @param element
	 *            objekt DealElement, ktory obsahuje vsetky hodnoty prvku z
	 *            ontologie.
	 * @return Dvojicu komponent a label == labelFor.
	 */
	private DealComponentPair getDealComponentPair(DealElement element) {
		DealComponentPair pair = null;
		DealComponent component1;
		DealComponent component2 = null;

		if ("JLabel".equals(element.getComponentClass())) {
			if (element.getLabel() != null) {
				component1 = new DealComponent(element.getId(), element.getComponentClass(), element.getLabel(), element.getParent(), element.getChilds());
			} else {
				component1 = new DealComponent(element.getId(), element.getComponentClass(), element.getName(), element.getParent(), element.getChilds());
			}
			pair = new DealComponentPair(component1);
		} else if (!"".equals(element.getComponentClass())) {
			component1 = new DealComponent(element.getId(), element.getComponentClass(), element.getName(), element.getParent(), element.getChilds());
			if (element.getLabel() != null && !"".equals(element.getLabel())) {
				component2 = new DealComponent(element.getId(), "JLabel", element.getLabel(), element.getParent(), element.getChilds());
			}
			pair = new DealComponentPair(component1, component2);
		}
		return pair;
	}

	/**
	 * odstrania sa komponenty ktore kopiruju parenta ako napr: ID: Gender ID:
	 * man componentClass: JLabel componentClass: JLabel text: Gender: text:
	 * Gender: parent: Person parent: Gender childs: [man, woman] childs: []
	 * 
	 * @param listOfComponents
	 *            zoznam komponentov z ktoreho sa maju podobne nespravne
	 *            vyextrahovane komponenty
	 */
	private void deleteFalseComponents(List<DealComponent> listOfComponents) {
		HashMap<DealComponent, DealComponent> componentsForDelete = new HashMap<>();

		for (DealComponent parentComponent : listOfComponents) {
			if (!parentComponent.getChilds().isEmpty()) {
				List<DealComponent> childsComponents = findChilds(parentComponent.getChilds(), listOfComponents);
				for (DealComponent child : childsComponents) {
					if (isFalseChild(parentComponent, child)) {
						componentsForDelete.put(child, child);
					}
				}
			}
		}
		listOfComponents.removeAll(componentsForDelete.values());
	}

	/**
	 * Overenie ci sa jedna o kopirujuce komponenty.
	 * 
	 * @param parent
	 *            objekt rodica
	 * @param child
	 *            objekt potomka
	 * @return true ak sa kopiruju, inac false.
	 */
	private boolean isFalseChild(DealComponent parent, DealComponent child) {
		if (parent.getComponentClass().equals(child.getComponentClass()) && parent.getText().equals(child.getText())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Najde konkretne komponenty v zozname ktore predstavuju dcerske elementy.
	 * 
	 * @param childs
	 *            zoznam mien dcerskych komponentov.
	 * @param list
	 *            zoznam vsetkych komponentov v ktorych sa maju hladat dcerske
	 *            komponenty.
	 * @return zoznam dcerskych elementov
	 */
	private List<DealComponent> findChilds(List<String> childs, List<DealComponent> list) {
		List<DealComponent> childsComponents = new ArrayList<>();
		for (DealComponent cmp : list) {
			for (String childId : childs) {
				if (cmp.getId().equals(childId)) {
					childsComponents.add(cmp);
				}
			}
		}
		return childsComponents;
	}

}
