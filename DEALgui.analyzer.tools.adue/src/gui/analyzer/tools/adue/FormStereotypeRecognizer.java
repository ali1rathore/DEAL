package gui.analyzer.tools.adue;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

import gui.analyzer.tools.adue.labelFor.ContainerWrapper;
import gui.analyzer.tools.adue.labelFor.Direction;
import gui.analyzer.tools.adue.labelFor.LabelWrapper;
import gui.analyzer.util.Logger;
import gui.model.domain.ComponentInfoType;
import gui.model.domain.DomainModel;
import gui.model.domain.Term;

/**
 * Searches for appropriate components for a JLabel without the labelFor
 * attribute. The results are the components, that should be set as the labelFor
 * attribute for this JLabel.
 * 
 * @author Martin Zbuska
 */
public class FormStereotypeRecognizer {

	/**
	 * HashMap represents the parent components - containers (key) and their
	 * child form and label components (values).
	 */
	HashMap<Component, ContainerWrapper> formContainers;

	/** Results of the recognizer will be stored here. */
	List<LabelWrapper> results = new ArrayList<LabelWrapper>();

	DomainModel domainModel;

	public FormStereotypeRecognizer() {
		this.formContainers = new HashMap<Component, ContainerWrapper>();
	}

	/**
	 * The method finds all JLabel components in the given domain model and if
	 * any of them has no labelFor attribute, it searches for an appropriate
	 * component to be set as a labelFor. The results are stored into the
	 * results list.
	 * 
	 * @param domainModel domain model containing all components of the tested
	 *        user interface.
	 */
	public void recognizeFormStereotypes(DomainModel domainModel) {
		this.domainModel = domainModel;

		clearAll();
		identifyFormContainers(domainModel.getAllTerms());

		// try all containers, whether they are form containers
		for (ContainerWrapper wrapper : this.formContainers.values()) {
			if (wrapper.getLabels().size() == 0 || wrapper.getComponents().size() == 0)
			    continue;

			findComponentsAroundLabel(wrapper);
			checkForStereotypes(wrapper.getLabels());
		}

		for (ContainerWrapper wrapper : this.formContainers.values()) {
			results.addAll(wrapper.getLabels());
		}
	}

	/**
	 * @return the results of the recognition is a list of JLabel components and
	 *         recommendations for their labelFor attribute.
	 */
	public List<LabelWrapper> getResults() {
		return results;
	}

	/**
	 * Clears all the lists.
	 */
	private void clearAll() {
		this.formContainers.clear();
		this.results.clear();
	}

	/**
	 * Fills the list of ComponentsAroundLabel objects by all labels from the
	 * domain model to recognize the form stereotypes. Should get the list
	 * of all terms from the domain model to be analyzed.
	 * 
	 * @param terms list of all terms to be analyzed
	 */
	private void identifyFormContainers(List<Term> terms) {
		HashMap<JLabel, JLabel> foundLabels = new HashMap<JLabel, JLabel>();
		List<LabelWrapper> tempListOfLabels;
		List<Component> tempListOfComponents;

		for (Iterator<Term> iter = terms.iterator(); iter.hasNext();) {
			Term t = iter.next();

			if (t.getComponentInfoType() != ComponentInfoType.CONTAINERS
			        && t.getComponent() instanceof Component) {
				Component c = (Component) t.getComponent();
				if (c == null) {
					Logger.logError("Term   ->  \"" + t + "\" nie je mozne pretypovat na Component.");
					continue;
				}

				// Riesenie na zaklade parenta
				Component container = c.getParent();
				if (!this.formContainers.containsKey(container)) {
					this.formContainers.put(container, new ContainerWrapper());
				}
				tempListOfLabels = this.formContainers.get(container).getLabels();
				tempListOfComponents = this.formContainers.get(container).getComponents();

				if (c instanceof JLabel) {
					foundLabels.put((JLabel) c, (JLabel) c);
				} else if (isFormComponent(c)) {
					tempListOfComponents.add(c);
					// Ak je k danemu komponentu priradeny label, treba dat do
					// zoznamu aj ten, aby bolo mozne urcit spravny stereotyp
					// formulara.
					JLabel label = t.getLabelForComponent();
					if (label != null) {
						foundLabels.put(label, label);
					}
				}
			}
		}

		// naplnenie zoznamu typu LabelWrapper jedinecnymi labelmy.
		// separedLabels je zoznam ktory je len docasny pre ziskavanie
		// jedinecnych labelov, ten poskytne info zoznamu listOfLabels
		if (foundLabels.isEmpty()) {
			System.out.println("Neboli najdene ziadne JLabel v domenovom modeli.");
		} else {
			for (JLabel label : foundLabels.values()) {
				tempListOfLabels = this.formContainers.get(label.getParent()).getLabels();
				tempListOfLabels.add(new LabelWrapper(label));
			}
		}
	}

	/**
	 * Finds the closest components for labels in all directions and sets their
	 * values to the corresponding directions in the ComponentsAroundLabel list.
	 */
	private void findComponentsAroundLabel(ContainerWrapper wrapper) {
		List<LabelWrapper> listOfLabels = wrapper.getLabels();
		List<Component> listOfComponents = wrapper.getComponents();

		for (LabelWrapper labelWrapper : listOfLabels) {
			JLabel label = labelWrapper.getLabel();

			for (Component component : listOfComponents) {
				if (!(component instanceof JSeparator)) {
					for (Direction d : Direction.values()) {
						if (d == Direction.MIX)
						    continue;

						JLabel nearestLabel = findNearestLabelNeighbour(label, d, listOfLabels);

						if (isIntersecting(label, nearestLabel, component, d)) {
							Component c = labelWrapper.getComponent(d);

							if (c == null || c.getX() > component.getX()) {
								labelWrapper.setComponent(component, d);
							}
						}
					}
				} else {
					System.out.println("Separator, pokracuj na dalsi komponent.");
				}
			}
		}
	}

	/**
	 * Finds the closest label in the given direction and it searches for a
	 * component maximally to this direction.
	 * 
	 * @param label JLabel component for which the closest label is searched for
	 * @param direction the direction in which the label is searched for
	 * @param listOfLabels list of all labels in the domain model
	 * @return the closest JLabel component in the given direction
	 */
	private JLabel findNearestLabelNeighbour(JLabel label, Direction direction,
	        List<LabelWrapper> listOfLabels) {
		Rectangle labelRect = new Rectangle(label.getX(), label.getY(), label.getWidth(), label.getHeight());
		Rectangle findingLabelRect;
		JLabel maxMove = null;

		for (LabelWrapper element : listOfLabels) {
			JLabel findingLabel = element.getLabel();
			if (label != findingLabel) {
				findingLabelRect = new Rectangle(findingLabel.getX(), findingLabel.getY(), findingLabel.getWidth(),
				        findingLabel.getHeight());
				labelRect.x = label.getX();
				labelRect.y = label.getY();

				switch (direction) {
					case RIGHT:
						while (labelRect.x < findingLabelRect.x) {
							labelRect.x++;
							if (labelRect.intersects(findingLabelRect) && maxMove == null
							        || labelRect.intersects(findingLabelRect) && maxMove.getX() > findingLabel.getX()) {
								maxMove = findingLabel;
							}
						}
						break;
					case LEFT:
						while (labelRect.x > findingLabelRect.x) {
							labelRect.x--;
							if (labelRect.intersects(findingLabelRect) && maxMove == null
							        || labelRect.intersects(findingLabelRect) && maxMove.getX() < findingLabel.getX()) {
								maxMove = findingLabel;
							}
						}
						break;
					case UP:
						while (labelRect.y > findingLabelRect.y) {
							labelRect.y--;
							if (labelRect.intersects(findingLabelRect) && maxMove == null
							        || labelRect.intersects(findingLabelRect) && maxMove.getY() < findingLabel.getY()) {
								maxMove = findingLabel;
							}
						}
						break;
					case DOWN:
						while (labelRect.y < findingLabelRect.y) {
							labelRect.y++;
							if (labelRect.intersects(findingLabelRect)
							        || labelRect.intersects(findingLabelRect) && maxMove.getY() > findingLabel.getY()) {
								maxMove = findingLabel;
							}
						}
						break;
					default:
						break;
				}
			}
		}
		return maxMove;
	}

	/**
	 * Finds out, if there is a component intersecting with the given label in
	 * the given direction and at the same time, the component is closer than
	 * the closest JLabel component in the given direction.
	 * 
	 * @param label for which we search a component
	 * @param nearestLabel the component has to be closer than the nearestLabel
	 *        in the given direction
	 * @param component for which the intersection is verified
	 * @param direction the direction, in which the intersection is verified
	 * @return true if there is an intersection, false otherwise
	 */
	private boolean isIntersecting(JLabel label, JLabel nearestLabel, Component component, Direction direction) {
		Rectangle labelRect = new Rectangle(label.getX(), label.getY(), label.getWidth(), label.getHeight());
		Rectangle compRect = new Rectangle(component.getX(), component.getY(), component.getWidth(),
		        component.getHeight());
		labelRect.x = label.getX();
		labelRect.y = label.getY();

		if (nearestLabel != null) {
			component = nearestLabel;
		}

		switch (direction) {
			case RIGHT:
				while (labelRect.x < component.getX()) {
					labelRect.x++;
					if (labelRect.intersects(compRect)) { return true; }
				}
				break;
			case LEFT:
				while (labelRect.x > component.getX()) {
					labelRect.x--;
					if (labelRect.intersects(compRect)) { return true; }
				}
				break;
			case UP:
				while (labelRect.y > component.getY()) {
					labelRect.y--;
					if (labelRect.intersects(compRect)) { return true; }
				}
				break;
			case DOWN:
				while (labelRect.y < component.getY()) {
					labelRect.y++;
					if (labelRect.intersects(compRect)) { return true; }
				}
				break;
			default:
				break;
		}
		return false;
	}

	/**
	 * Counts the number of components in each direction from each label and
	 * based on the number decides, which component is assigned to which label.
	 * 
	 * @param listOfLabels the list of all labels in the domain model
	 */
	private void checkForStereotypes(List<LabelWrapper> listOfLabels) {
		Integer[] counts = new Integer[] { 0, 0, 0, 0 };

		Direction[] directions = Direction.values();

		for (LabelWrapper labelWrapper : listOfLabels) {
			for (int i = 0; i < directions.length - 1; i++) {
				if (labelWrapper.getComponent(directions[i]) != null) {
					counts[i] = counts[i] + 1;
				}
			}
		}
		// System.out.println("\n\nRight = " + rightCount + "\nLeft = " +
		// leftCount + "\nUp = " + upCount + "\nDown = " + downCount);

		Logger.log("\n\n DomainModel: " + domainModel.getName());
		for (int i = 0; i < directions.length - 1; i++) {
			Direction d = directions[i];

			if (d == Direction.MIX) continue;
			Logger.log(d.toString() + " = " + counts[i]);
		}

		Direction stereotypeDirection = determineStereotype(counts);
		Logger.log("\nStereotype: " + stereotypeDirection);

		// System.out.println("\nStereotype: " + stereotypeDirection);

		// Pred tym ako sa priradia komponenty k labelom odstrania sa uz
		// programatorom definovane Labely s labelFor atributom
		Iterator<LabelWrapper> iterator = listOfLabels.iterator();
		while (iterator.hasNext()) {
			LabelWrapper labelWrapper = iterator.next();
			if (labelWrapper.getLabelForComponent() != null) {
				iterator.remove();
			}
		}

		assignLabelForComponent(stereotypeDirection, listOfLabels);

		// printListOfLabels(listOfLabels);
	}

	/**
	 * Decides which stereotype was recognized. Based on that the following
	 * steps are executed.
	 * 
	 * @param counts the number of components in four directions from the label
	 *        in an array, sorted according to the Direction enum values.
	 * @return the direction of the form stereotype
	 */
	private Direction determineStereotype(Integer[] counts) {
		List<Integer> temp = Arrays.asList(counts);
		Arrays.sort(counts);

		int max = counts[3];

		return max == counts[2] ? Direction.MIX : Direction.values()[temp.indexOf(max)];
	}

	/**
	 * For each label or element from the listOfLabel assigns a component into
	 * the labelForComponent variable. This variable contains an object of the
	 * component, for which the label is If there is no component around the
	 * label, the labelForComponent value is set to null.
	 * 
	 * @param direction the direction in which the component should be assigned
	 * @param listOfLabels list of all labels in the domain model
	 */
	private void assignLabelForComponent(Direction direction, List<LabelWrapper> listOfLabels) {
		for (LabelWrapper label : listOfLabels) {
			// Do only for labels without labelFor set
			Component c = label.getLabel().getLabelFor();

			if (c == null) {
				c = label.getComponent(direction);
				if (direction == Direction.MIX) {
					c = getOptimalComponent(label);
				}

			}

			label.setLabelForComponent(c);
		}
	}

	/**
	 * Finds the closest component from the list of components found for a
	 * label.
	 * 
	 * @param l object of type ComponentsAroundLabel, which contains all data
	 *        to work with.
	 * @return the closest component to the label.
	 */
	private Component getOptimalComponent(LabelWrapper l) {
		Component optimalComponent = null;

		for (Direction d : Direction.values()) {
			if (d == Direction.MIX) continue;

			Component c = l.getComponent(d);

			if (c != null) {
				if (optimalComponent == null) {
					optimalComponent = c;
					continue;
				}

				int newComponentDistance = getComponentDistance(l.getLabel(), c);
				int optimalComponentDistance = getComponentDistance(l.getLabel(), optimalComponent);
				if (newComponentDistance < optimalComponentDistance) {
					optimalComponent = c;
				}
			}
		}

		return optimalComponent;
	}

	/**
	 * @param l label component
	 * @param c the component to which the distance should be measured
	 * @return the distance between the label and the component in the UI.
	 */
	private int getComponentDistance(JLabel l, Component c) {
		Rectangle labelRect = new Rectangle(l.getX(), l.getY(), l.getWidth(), l.getHeight());
		Rectangle compRect = new Rectangle(c.getX(), c.getY(), c.getWidth(), c.getHeight());
		int distance = 0;

		while (!labelRect.intersects(compRect)) {
			labelRect.x -= 1;
			labelRect.y -= 1;
			labelRect.width += 2;
			labelRect.height += 2;
			distance++;
		}
		return distance;
	}

	/**
	 * Verifies, if the component is a form-type component.
	 * 
	 * @param c tested component
	 * @return true if the component is a form-tupe component false otherwise.
	 */
	private boolean isFormComponent(Component c) {
		return (c instanceof JTextComponent
		        || c instanceof JToggleButton
		        || c instanceof JComboBox
		        || c instanceof JSlider
		        || c instanceof JSpinner
		        || c instanceof JTable
		        || c instanceof JList
		        || c instanceof JLabel && ((JLabel) c).getIcon() != null);
	}
}
