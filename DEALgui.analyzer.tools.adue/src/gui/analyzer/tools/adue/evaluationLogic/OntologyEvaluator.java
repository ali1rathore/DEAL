/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.evaluationLogic;

import gui.analyzer.tools.adue.evaluationLogic.results.ElementResultsWrapper;
import gui.analyzer.tools.adue.evaluationLogic.results.OneOntologyResults;
import gui.analyzer.tools.adue.evaluationLogic.results.TwoOntologiesResults;
import gui.analyzer.tools.adue.owl.OWLParser;
import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealComponentPair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * Trieda zabezpecujuca hodnotenie ontologii na zaklade extrahovanych
 * KOMPONENTOV z ontologie. Dedi od triedy AbstractEvaluator, ktora obsahuje
 * metody na hodnotenie gramatiky a rodicovskych pojmov.
 * 
 * @author Martin Zbuska
 */
public class OntologyEvaluator extends AbstractEvaluator {

	/**
	 * Zoznam poloziek extrahovanych zo starej ontologie.
	 */
	private final List<DealComponent> oldComponents;
	
	/**
	 * Zoznam poloziek extrahovanych z novej ontologie.
	 */
	private final List<DealComponent> newComponents;

	/**
	 * Objekt reprezentujuci vysledky porovnania dvoch ontologii.
	 */
	private TwoOntologiesResults twoOntologiesResults;
	
	/**
	 * Objekt reprezentujuci vysledky kontroly gramatiky a rodicovskych pojmov.
	 */
	private OneOntologyResults oneOntologyResults;

	/**
	 * Konstruktor triedy OntologyEvaluator ak sa budu hodnotit dve rozlicne
	 * otnologie.
	 * 
	 * @param oldOntologyPath
	 *            cesta k starej ontologii podla ktorej sa bude hodnotit.
	 * @param newOntologyPath
	 *            cesta k novej ontologii ktora ma byt hodnotena.
	 * @param fullCheck
	 *            true ak sa ma overovat gramatika a spravnost rodicov pri
	 *            vsetkych komponentoch novej ontologie, false ak sa ma overovat
	 *            gramatika a spravnost rodicov len pri novych komponentoch
	 *            novej ontologie.
	 * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
	 */
	public OntologyEvaluator(String oldOntologyPath, String newOntologyPath, boolean fullCheck) throws OWLOntologyCreationException {
		super(fullCheck);
		OWLParser parser = OWLParser.getInstance();
		this.oldComponents = parser.getListOfComponentsFromOntology(oldOntologyPath);
		this.newComponents = parser.getListOfComponentsFromOntology(newOntologyPath);
	}

	/**
	 * Konstruktor triedy OntologyEvaluator ak sa bude hodnotit len jedna nova
	 * ontologia.
	 * 
	 * @param ontologyPath
	 *            cesta k ontologii ktora sa ma hodnotit
	 * @param fullCheck
	 *            true ak sa ma overovat gramatika a spravnost rodicov pri
	 *            vsetkych komponentoch novej ontologie, false ak sa ma overovat
	 *            gramatika a spravnost rodicov len pri novych komponentoch
	 *            novej ontologie.
	 * @throws OWLOntologyCreationException
	 */
	public OntologyEvaluator(String ontologyPath, boolean fullCheck) throws OWLOntologyCreationException {
		super(fullCheck);
		OWLParser parser = OWLParser.getInstance();
		this.oldComponents = null;
		this.newComponents = parser.getListOfComponentsFromOntology(ontologyPath);
	}

	/**
	 * Metoda vykonavajuca hodnotenie ontologii. Rozhoduje medzi jednou a dvoma
	 * ontologiami na zaklade toho, ktory konstuktor tejto triedy bol pouzity.
	 */
	public void evaluate() {
		if (this.getOldComponents() != null) {
			// Vykonanie procesu hodnotenia dvoch ontologii
			this.twoOntologiesResults = this.evaluateTwoOntologies(this.getOldComponents(), this.getNewComponents());
			this.oneOntologyResults = evaluateNewOntology(this.getTwoOntologiesResults().getListOfNewComponents());
			// printResults(twoOntologiesResults);
			// printOneResults(oneOntologyResults);
		} else {
			// Vykonanie procesu hodnotenia novej ontologie
			this.twoOntologiesResults = null;
			this.oneOntologyResults = this.evaluateNewOntology(this.getNewComponents());
			// printOneResults(oneOntologyResults);
		}
	}

	/**
	 * Vyhodnoti a vytvori vysledky z dvoch ontologii. Vyhodnocovanie je v troch
	 * krokoch. 1 krok = zistuju sa komponenty ktore boli opatovne pouzite (to
	 * je OK) zvysne komponenty sa hodnotia v druhom kroku 2 krok = zistuje sa
	 * ktore komponenty boli zmenene oproti starim, hodnoti sa ci boli zmenene
	 * podla definovanych pravidiel, ci zmena je vhodna 3 krok = zvysne
	 * komponenty su nove cize tzv. nova ontologia novej aplikacie, a zo starych
	 * komponentov su to nepouzite komponenty co tiez nemusi byt vhodne
	 * 
	 * @param oldComponents
	 *            zoznam komponentov zo starej aplikacie/ontologie
	 * @param newComponents
	 *            zoznam komponentov v novej aplikacii/ontologie
	 * @return objekt reprezentujuci vysledky porovnania dvoch ontologii.
	 */
	private TwoOntologiesResults evaluateTwoOntologies(List<DealComponent> oldComponents, List<DealComponent> newComponents) {
		TwoOntologiesResults results = new TwoOntologiesResults();
		// vytvorenie kopii zoznamov kedze sa z nich bude mazat
		List<DealComponent> copyOfOldComponents = new ArrayList<>(oldComponents);
		List<DealComponent> copyOfNewComponents = new ArrayList<>(newComponents);

		// 1 krok == zistovanie kolko a ktore polozky boli zachovane oproti
		// starej ontologii
		Iterator<DealComponent> iterator = copyOfOldComponents.iterator();
		while (iterator.hasNext()) {
			DealComponent oldComponent = iterator.next();
			DealComponent contained = oldComponent.isContainedInList(copyOfNewComponents);
			if (contained != null) {
				// if(copyOfNewComponents.contains(oldComponent)) {
				// nastavenie results
				results.getListOfRetainedOldComponents().add(contained);// .add(oldComponent);
																		// ->
																		// toto
																		// bolo
																		// chybne
																		// potom
				// zmazanie uz overenych a obsiahnutyc komponentov z oboch
				// zoznamov
				copyOfNewComponents.remove(contained);
				// copyOfNewComponents.remove(oldComponent);
				iterator.remove();
			}
		}
		// koniec 1 kroku
		setProgress(25);
		// 2 krok == zistenie v ktory polozkach nastala zmena oproti starej
		// ontologii. Zistovanie na zaklade textu, zmeneny len typ komponentu.
		iterator = copyOfOldComponents.iterator();
		while (iterator.hasNext()) {
			DealComponent oldComponent = iterator.next();
			// Ziska zoznam komponentov ktore maju rovnaky text ako oldComponent
			List<DealComponent> changes = getChanges(oldComponent, copyOfNewComponents);

			if (changes != null) {
				// Ak sa najde prva spravna zmena, zapise sa to, a pokracuje sa
				// v kroku 2 na dalsi prvok.
				DealComponent correctChangedComponent = this.getCorrectChange(oldComponent, changes);
				if (correctChangedComponent == null) {
					results.getListOfAdverseChangedComponents().add(new DealComponentPair(oldComponent, changes.get(0)));
					copyOfNewComponents.remove(changes.get(0));
				} else {
					results.getListOfGoodChangedComponents().add(new DealComponentPair(oldComponent, correctChangedComponent));
					copyOfNewComponents.remove(correctChangedComponent);
				}
				// ak existovala co len jedna zmena tak stary komponent bol
				// zmeneny teda sa ho moze odstranit
				iterator.remove();
			}
		}
		// koniec 2 kroku
		setProgress(50);
		// 3 krok == zvysne komponenty v novej ontologii su nove a tie treba
		// vyhodnotit ako samostatnu ontologiu, zvysne stare komponenty neboli v
		// novej ontologii pouzite
		results.getListOfDeletedComponents().addAll(copyOfOldComponents);
		results.getListOfNewComponents().addAll(copyOfNewComponents);
		// koniec 3 kroku
		setProgress(75);
		return results;
	}

	/**
	 * Vyhodnoti jednu samostantu ontologiu, vytvori objekt s vysledkami.
	 * 
	 * @param newOntologyComponents
	 *            zoznam prvkov s ich vlastnostami, novej ontologie
	 * @return objekt s vysledkami hodnotenia
	 */
	private OneOntologyResults evaluateNewOntology(List<DealComponent> newOntologyComponents) {
		// Nastavi ci sa ma overovat gramatika a rodicia vo vsetkych
		// komponentoch novej ontologie alebo len v tych novych.
		List<DealComponent> componentsForCheck;
		if (fullCheck) {
			componentsForCheck = newComponents;
		} else {
			componentsForCheck = newOntologyComponents;
		}

		OneOntologyResults results = new OneOntologyResults();
		String correction;
		for (DealComponent component : componentsForCheck) {
			if (component.getText() != null) {
				correction = getGrammarCorrection(component.getText());
			} else {
				correction = null;
			}
			if (correction != null) {
				results.getListOfGrammaticalErrors().add(new ElementResultsWrapper(component, component.getText(), correction));
			}

			if (component.getId() != null && !component.getChilds().isEmpty()) {
				// id komponentu predstavuje nazov rodica pre deti tohoto
				// komponentu, takze predstavuje tento komponent ako rodica
				correction = checkParent(component.getId(), component.getChilds());
				if (correction != null) {
					// id komponentu predstavuje nazov rodica pre deti tohoto
					// komponentu, takze predstavuje tento komponent ako rodica
					results.getListOfWrongParents().add(new ElementResultsWrapper(component, component.getId(), correction));
				}
			}
		}
		setProgress(100);
		return results;
	}

	/**
	 * Zisti komponent ktory splna podmienku spravnej zmeny komponentu a zaroven
	 * je svojou urovnou co najblizsie ku staremu komponentu.
	 * 
	 * @param oldComponent
	 *            stary komponent podla ktoreho sa urcuje spravnost zmeny
	 * @param changedComponents
	 *            zoznam novych komponentov s rovnakym textom ale inym typom
	 *            komponentu.
	 * @return null ak sa nenasla ziadna spravna zmena, inak objekt
	 *         DealComponent ktory bol spravne zmeneny a je levelom co
	 *         najblizsie ku staremu komponentu.
	 */
	private DealComponent getCorrectChange(DealComponent oldComponent, List<DealComponent> changedComponents) {
		DealComponent smallestChange = null;

		for (DealComponent component : changedComponents) {
			if (this.getComponentLevel(oldComponent.getComponentClass()) <= this.getComponentLevel(component.getComponentClass())) {
				if (smallestChange == null) {
					smallestChange = component;
				} else if (this.getComponentLevel(component.getComponentClass()) <= this.getComponentLevel(smallestChange.getComponentClass())) {
					smallestChange = component;
				}
			}
		}
		return smallestChange;
	}

	/**
	 * Zisti uroven komponentu. Potrebne pri overovani korektnej zmeny medzi
	 * ontologiami
	 * 
	 * @param componentClass
	 *            v textovej forme reprezentovany typ kontrolovaneho komponentu
	 * @return cislo reprezentujuce uroven typu komponentu. Cim vyssie cislo tym
	 *         vyssia uroven.
	 */
	private int getComponentLevel(String componentClass) {
		switch (componentClass) {
		case "JSeparator":
		case "JScrollBar":
			return 0;
		case "JLabel":
		case "JProgressBar":
			return 1;
		case "JTextField":
		case "JTextArea":
		case "JFormattedTextField":
		case "JPasswordField":
		case "JSpinner":
			return 2;
		case "JSlider":
			return 3;
		case "JCheckBox":
		case "JCheckBoxMenuItem":
			return 4;
		case "JRadioButton":
		case "JRadioButtonMenuItem":
			return 5;
		case "ButtonGroup":
			return 6;
		case "JComboBox":
		case "JList":
		case "JTree":
		case "JTable":
			return 7;
		case "JButton":
		case "JToggleButton":
		case "JMenuItem":
		case "JMenu":
			return 8;
		case "JTextPane":
		case "JEditorPane":
		case "JPanel":
		case "JTabbedPane":
		case "JSplitPane":
		case "JScrollPane":
		case "JToolBar":
		case "JDesktopPane":
		case "JInternalFrame":
		case "JLayeredPane":
		case "JMenuBar":
		case "JPopupMenu":
			return 9;
		default:
			// Ak je to neznamy komponent nie je mozne urcit ci je vhodne ho
			// menit.
			// Preto je najvhodnejsie vyhodnotit ze je najvyssej urovne a teda
			// je lepsie nemenit jeho typ.
			return 100;
		}
	}

	/**
	 * Ziska zoznam poloziek v novej ontologii, ktore maju rovnaky text ako
	 * stara polozka ale je ineho typu.
	 * 
	 * @param oldComponent
	 *            polozka starej ontologie, pre ktoru sa hladaju polozky s
	 *            rovnakym textom v novej ontologii.
	 * @param copyOfNewComponents
	 *            polozky novej ontologie, ktore su ine ako tie v starej.
	 * @return zoznam zmenenych poloziek
	 */
	private List<DealComponent> getChanges(DealComponent oldComponent, List<DealComponent> copyOfNewComponents) {
		List<DealComponent> changesInOldComponent = null;

		for (DealComponent testedComponent : copyOfNewComponents) {
			if (oldComponent.getText() != null) {
				if (oldComponent.getText().equals(testedComponent.getText())) {
					if (changesInOldComponent == null) {
						changesInOldComponent = new ArrayList<>();
					}
					changesInOldComponent.add(testedComponent);
				}
			}
		}
		return changesInOldComponent;
	}

	/**
	 * @return the twoOntologiesResults
	 */
	public TwoOntologiesResults getTwoOntologiesResults() {
		return twoOntologiesResults;
	}

	/**
	 * @return the oneOntologyResults
	 */
	public OneOntologyResults getOneOntologyResults() {
		return oneOntologyResults;
	}

	/**
	 * Prints the results of evaluating two ontologies into console.
	 * 
	 * @param results
	 */
	@SuppressWarnings("unused")
	private void printResults(TwoOntologiesResults results) {
		System.out.println("Pocet ponechanych prvkov: " + results.getListOfRetainedOldComponents().size());
		for (DealComponent result : results.getListOfRetainedOldComponents()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "   " + result.getParent() + "   " + result.getChilds());
		}
		System.out.println("Pocet novych prvkov: " + results.getListOfNewComponents().size());
		for (DealComponent result : results.getListOfNewComponents()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "   " + result.getParent() + "   " + result.getChilds());
		}
		System.out.println("Pocet zmazanych prvkov: " + results.getListOfDeletedComponents().size());
		for (DealComponent result : results.getListOfDeletedComponents()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "   " + result.getParent() + "   " + result.getChilds());
		}
		System.out.println("Pocet zmenenych prvkov: " + (results.getListOfGoodChangedComponents().size() + results.getListOfAdverseChangedComponents().size()));
		System.out.println("    Pocet spravne zmenenych prvkov: " + results.getListOfGoodChangedComponents().size());
		for (DealComponentPair pair : results.getListOfGoodChangedComponents()) {
			DealComponent result = pair.getComponent1();
			DealComponent changed = pair.getComponent2();
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "   " + result.getParent() + "   " + result.getChilds());
			System.out.println("                          -> " + changed.getId() + "   " + changed.getComponentClass() + "   " + changed.getText() + "   " + changed.getParent() + "   " + changed.getChilds());
		}
		System.out.println("    Pocet NEspravne zmenenych prvkov: " + results.getListOfAdverseChangedComponents().size());
		for (DealComponentPair pair : results.getListOfAdverseChangedComponents()) {
			System.out.println("         " + pair.getComponent1().getComponentClass() + " = " + pair.getComponent1().getText() + "     --->     " + pair.getComponent2().getComponentClass() + " = " + pair.getComponent2().getText());

		}
	}

	/**
	 * @return the oldComponents
	 */
	public List<DealComponent> getOldComponents() {
		return oldComponents;
	}

	/**
	 * @return the newComponents
	 */
	public List<DealComponent> getNewComponents() {
		return newComponents;
	}

	/**
	 * Metoda, ktora zabezpecuje spustenie hodnotenia v osobitnom vlakne.
	 */
	@Override
	protected AbstractEvaluator doInBackground() throws Exception {
		evaluate();
		return this;
	}

}
