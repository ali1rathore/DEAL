/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.evaluationLogic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.swabunga.spell.engine.SpellDictionaryHashMap;
import com.swabunga.spell.engine.Word;
import com.swabunga.spell.event.SpellCheckEvent;
import com.swabunga.spell.event.SpellCheckListener;
import com.swabunga.spell.event.SpellChecker;
import com.swabunga.spell.event.StringWordTokenizer;
import com.swabunga.spell.event.TeXWordFinder;

/**
 * Trieda reprezentujuca kotnrolu gramatickych chyb. Implementuje metody z
 * rozhrania SpellCheckListener kniznice Jazzy.
 * 
 * @author Martin Zbuska
 */
public class DealSpellChecker implements SpellCheckListener {

	/**
	 * Objekt kniznice Jazzy, vykonavajuci kontrolu gramatiky.
	 */
	private SpellChecker spellChecker;
	
	/**
	 * Zoznam slov s gramatickou chybou.
	 */
	private final List<String> misspelledWords;
	
	/**
	 * Cesta k suboru, reprezentujuceho gramaticky slovnik.
	 */
	private static String dictionaryPath = 
			new File(".").getAbsolutePath().concat("\\dictionaries\\EN - us.txt");

	/**
	 * Konstruktor.
	 */
	public DealSpellChecker() {
		initialize();
		this.misspelledWords = new ArrayList<>();
	}

	/**
	 * Inicializuje spellChecker aby s nim bolo mozne pracovat. Nacita slovnik
	 * zo suboru.
	 */
	private void initialize() {
		File dictionaryFile = new File(dictionaryPath);
		SpellDictionaryHashMap dictionaryHashMap;
		try {
			dictionaryHashMap = new SpellDictionaryHashMap(dictionaryFile);
			this.spellChecker = new SpellChecker(dictionaryHashMap);
			this.spellChecker.addSpellCheckListener(this);
		} catch (IOException ex) {
			Logger.getLogger(DealSpellChecker.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Nastavi cestu k novemu slovniku.
	 * 
	 * @param path
	 *            cesta k suboru reprezentujuceho novy slovnik.
	 */
	public static void setDictionary(String path) {
		DealSpellChecker.dictionaryPath = path;
	}

	/**
	 * @return cesta k aktualne nastavenemu slovniku.
	 */
	public static String getDictionaryPath() {
		return DealSpellChecker.dictionaryPath;
	}

	/**
	 * Overridnuta metoda na pridavanie chybnych slov do zoznamu. Nemenit jej
	 * obsah.
	 * 
	 * @param event
	 */
	@Override
	public void spellingError(SpellCheckEvent event) {
		event.ignoreWord(true);
		misspelledWords.add(event.getInvalidWord());
	}

	/**
	 * Vrati zoznam slov, v ktorych je chyba. Napr "wht" == "what" cize vrati
	 * slovo "wht".
	 * 
	 * @param text
	 *            text potrebny na kontrolu.
	 * @return slova textu, ktore obsahuju gramaticku chybu.
	 */
	public List<String> getMisspelledWords(String text) {
		// Overovanie gramatiky.
		// misspelledWords.clear();
		StringWordTokenizer texTok = new StringWordTokenizer(text, new TeXWordFinder());
		this.spellChecker.checkSpelling(texTok);
		return this.misspelledWords;
	}

	/**
	 * Vrati zoznam slov, ktore su navrhy na opravu zleho slova.
	 * 
	 * @param misspelledWord
	 * @return zoznam slov
	 */
	public List<String> getSuggestions(String misspelledWord) {
		@SuppressWarnings("unchecked")
		List<Word> suggestions = this.spellChecker.getSuggestions(misspelledWord, 0);
		List<String> results = new ArrayList<>();
		for (Word suggestion : suggestions) {
			results.add(suggestion.getWord());
		}
		return results;
	}

	/**
	 * Premaze zoznam slov s gramatickymi chybami, aby sa tieto slova
	 * neopakovali aj tam kde nie su.
	 */
	public void clearMisspelledWords() {
		this.misspelledWords.clear();
	}
}
