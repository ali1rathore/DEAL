/**
 * Tento balik zahrna triedy predstavujuce logicku cast porovnavania a hodnotenia ontologii.
 * Obsahuje balik s nazvom results, ktory predstavuje triedy uchovavajuce vysledky hodnoteni.
 */
package gui.analyzer.tools.adue.evaluationLogic;