/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.evaluationLogic;

import gui.analyzer.tools.adue.evaluationLogic.results.ElementResultsWrapper;
import gui.analyzer.tools.adue.evaluationLogic.results.OneOntologyResults;
import gui.analyzer.tools.adue.evaluationLogic.results.SimpleTwoOntologiesResults;
import gui.analyzer.tools.adue.owl.OWLParser;
import gui.analyzer.tools.adue.owl.extracted.DealElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 * Trieda zabezpecujuca hodnotenie ontologii na zaklade extrahovanych POLOZIEK z
 * ontologie. Dedi od triedy AbstractEvaluator, ktora obsahuje metody na
 * hodnotenie gramatiky a rodicovskych pojmov.
 * 
 * @author Martin Zbuska
 */
public class SimpleOntologyEvaluator extends AbstractEvaluator {

	/**
	 * Zoznam extrahovanych poloziek zo starej ontologie.
	 */
	private final List<DealElement> oldElements;
	
	/**
	 * Zoznam extrahovanych poloziek z novej ontologie.
	 */
	private final List<DealElement> newElements;

	/**
	 * Objekt reprezentujuci vysledky porovnania ontologii.
	 */
	private SimpleTwoOntologiesResults simpleTwoOntologiesResults;
	
	/**
	 * Objekt reprezentujuci vysledky kontroly gramatiky a rodicovskych pojmov.
	 */
	private OneOntologyResults simpleOneOntologyResults;

	/**
	 * Konstruktor triedy SimpleOntologyEvaluator ak sa budu hodnotit dve
	 * rozlicne otnologie jednoduchym sposobom.
	 * 
	 * @param oldOntologyPath
	 *            cesta k starej ontologii podla ktorej sa bude hodnotit.
	 * @param newOntologyPath
	 *            cesta k novej ontologii ktora ma byt hodnotena.
	 * @param fullCheck
	 *            true ak sa ma overovat gramatika a spravnost rodicov pri
	 *            vsetkych elementoch novej ontologie, false ak sa ma overovat
	 *            gramatika a spravnost rodicov len pri novych elementoch novej
	 *            ontologie.
	 * @throws OWLOntologyCreationException
	 */
	public SimpleOntologyEvaluator(String oldOntologyPath, String newOntologyPath, boolean fullCheck) throws OWLOntologyCreationException {
		super(fullCheck);
		OWLParser parser = OWLParser.getInstance();
		this.oldElements = parser.getListOfOntologyElements(oldOntologyPath);
		this.newElements = parser.getListOfOntologyElements(newOntologyPath);
	}

	/**
	 * Konstruktor triedy SimpleOntologyEvaluator ak sa bude hodnotit len jedna
	 * nova ontologia.
	 * 
	 * @param ontologyPath
	 *            cesta k ontologii ktora sa ma hodnotit
	 * @param fullCheck
	 *            true ak sa ma overovat gramatika a spravnost rodicov pri
	 *            vsetkych elementoch novej ontologie, false ak sa ma overovat
	 *            gramatika a spravnost rodicov len pri novych elementoch novej
	 *            ontologie.
	 * @throws OWLOntologyCreationException
	 */
	public SimpleOntologyEvaluator(String ontologyPath, boolean fullCheck) throws OWLOntologyCreationException {
		super(fullCheck);
		OWLParser parser = OWLParser.getInstance();
		this.oldElements = null;
		this.newElements = parser.getListOfOntologyElements(ontologyPath);
	}

	/**
	 * Metoda vykonavajuca hodnotenie ontologii. Rozhoduje medzi jednou a dvoma
	 * ontologiami na zaklade pouziteho konstruktora.
	 */
	public void evaluate() {
		if (this.getOldElements() != null) {
			// Vykonanie procesu hodnotenia dvoch ontologii
			this.simpleTwoOntologiesResults = simpleCompareTwoOntologies(getOldElements(), getNewElements());
			this.simpleOneOntologyResults = simpleEvaluateNewOntology(this.simpleTwoOntologiesResults.getListOfNewElements());
			// printResults(this.simpleTwoOntologiesResults);
			// printOneResults(simpleOneOntologyResults);
		} else {
			// Vykonanie procesu hodnotenia novej ontologie
			this.simpleTwoOntologiesResults = null;
			this.simpleOneOntologyResults = simpleEvaluateNewOntology(this.getNewElements());
			// printOneResults(simpleOneOntologyResults);
		}
	}

	/**
	 * Jednoduche porovnanie dvoch ontologii na zaklade vyparsovanych ELEMENTOV
	 * (DealElement). Porovnanie element == element Porovnavanie je v dvoch
	 * krokoch. 1 krok = hladaju sa identicke polozky medzi oboma ontologiami. 2
	 * krok = vsetky zvysne komponenty su brane nasledovne: - zo starej
	 * ontologie su oznacene ako zmazane polozky - z novej ontologie su oznacene
	 * ako nove polozky
	 * 
	 * @param oldElements
	 *            zoznam elementov zo starej aplikacie/ontologie
	 * @param newElements
	 *            zoznam elementov v novej aplikacii/ontologie
	 * @return objekt reprezentujuci vysledky porovnania dvoch ontologii.
	 */
	private SimpleTwoOntologiesResults simpleCompareTwoOntologies(List<DealElement> oldElements, List<DealElement> newElements) {
		SimpleTwoOntologiesResults results = new SimpleTwoOntologiesResults();
		// vytvorenie kopii zoznamov kedze sa z nich bude mazat
		List<DealElement> copyOfOldElements = new ArrayList<>(oldElements);
		List<DealElement> copyOfNewElements = new ArrayList<>(newElements);

		// 1 krok ... vybratie ktore prvky sa ponechali
		Iterator<DealElement> iterator = copyOfOldElements.iterator();
		while (iterator.hasNext()) {
			DealElement oldElement = iterator.next();
			if (copyOfNewElements.contains(oldElement)) {
				results.getListOfRetainedOldElements().add(oldElement);
				copyOfNewElements.remove(oldElement);
				iterator.remove();
			}
		}
		// koniec 1 kroku
		setProgress(30);
		// 2 krok zvysne elementy v oboch zoznamoch su pouzite priamo ako
		// vysledky. Zo starych elementov su definovane vsetky ako zmazane, v
		// novom zozname su nove prvky.
		results.setListOfDeletedElements(copyOfOldElements);
		results.setListOfNewElements(copyOfNewElements);
		// koniec 2 kroku
		setProgress(60);
		return results;
	}

	/**
	 * Vyhodnoti jednu samostatnu ontologiu.
	 * 
	 * @param newOntologyElements
	 *            zoznam poloziek na vyhodnotenie.
	 * @return objekt reprezentujuci vysledky hodnotenia.
	 */
	private OneOntologyResults simpleEvaluateNewOntology(List<DealElement> newOntologyElements) {
		// Nastavi ci sa ma overovat gramatika a rodicia vo vsetkych elementoch
		// novej ontologie alebo len v tych novych.
		List<DealElement> elementsForCheck;
		if (fullCheck) {
			elementsForCheck = newElements;
		} else {
			elementsForCheck = newOntologyElements;
		}

		OneOntologyResults results = new OneOntologyResults();
		String correction;
		for (DealElement element : elementsForCheck) {
			if (element.getLabel() != null && !"".equals(element.getLabel())) {
				correction = getGrammarCorrection(element.getLabel());
				if (correction != null) {
					results.getListOfGrammaticalErrors().add(new ElementResultsWrapper(element, element.getLabel(), correction));
				}
			} else if (element.getName() != null && !"".equals(element.getName())) {
				correction = getGrammarCorrection(element.getName());
				if (correction != null) {
					results.getListOfGrammaticalErrors().add(new ElementResultsWrapper(element, element.getName(), correction));
				}
			}

			if (element.getId() != null && !element.getChilds().isEmpty()) {
				correction = checkParent(element.getId(), element.getChilds());
			} else {
				correction = null;
			}
			if (correction != null) {
				results.getListOfWrongParents().add(new ElementResultsWrapper(element, element.getParent(), correction));
			}
		}
		setProgress(100);
		return results;
	}

	/**
	 * @return the simpleTwoOntologiesResults
	 */
	public SimpleTwoOntologiesResults getSimpleTwoOntologiesResults() {
		return simpleTwoOntologiesResults;
	}

	/**
	 * @return the simpleOneOntologyResults
	 */
	public OneOntologyResults getSimpleOneOntologyResults() {
		return simpleOneOntologyResults;
	}

	/**
	 * Vypise vysledky hodnotenia dvoch ontologii do konzoly.
	 * 
	 * @param results
	 */
	@SuppressWarnings("unused")
	private void printResults(SimpleTwoOntologiesResults results) {
		System.out.println("Pocet ponechanych prvkov: " + results.getListOfRetainedOldElements().size());
		for (DealElement result : results.getListOfRetainedOldElements()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getName() + "   " + result.getLabel() + "   " + result.getDescription() + "   " + result.getParent() + "   " + result.getChilds());
		}
		System.out.println("Pocet novych prvkov: " + results.getListOfNewElements().size());
		for (DealElement result : results.getListOfNewElements()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getName() + "   " + result.getLabel() + "   " + result.getDescription() + "   " + result.getParent() + "   " + result.getChilds());
		}
		System.out.println("Pocet zmazanych prvkov: " + results.getListOfDeletedElements().size());
		for (DealElement result : results.getListOfDeletedElements()) {
			System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getName() + "   " + result.getLabel() + "   " + result.getDescription() + "   " + result.getParent() + "   " + result.getChilds());
		}
	}

	/**
	 * @return the oldElements
	 */
	public List<DealElement> getOldElements() {
		return oldElements;
	}

	/**
	 * @return the newElements
	 */
	public List<DealElement> getNewElements() {
		return newElements;
	}

	/**
	 * Metoda, ktora zabezpecuje spustenie hodnotenia v osobitnom vlakne.
	 */
	@Override
	protected AbstractEvaluator doInBackground() throws Exception {
		evaluate();
		return this;
	}

}
