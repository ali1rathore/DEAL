package gui.analyzer.tools.adue.evaluationLogic;

import java.util.List;

import gui.analyzer.tools.adue.evaluationLogic.results.OneOntologyResults;
import gui.analyzer.tools.adue.evaluationLogic.results.SimpleTwoOntologiesResults;
import gui.analyzer.tools.adue.evaluationLogic.results.TwoOntologiesResults;
import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.ui.observable.AdueResults;

public class DuMetricsCalculator {
	private final float WEIGHT_DOMAIN_CONTENT = 2.9f;
	private final float WEIGHT_DOMAIN_SPECIFICITY = 2.6f;
	private final float WEIGHT_CONSISTENCY = 2.6f;
	private final float WEIGHT_LANGUAGE_ERRORS_AND_BARRIERS = 1.7f;
	private final float WEIGHT_WORLD_LANGUAGE = 1.54f;
	private float domainContent = 0, domainSpecificity = 0, consistency = 0, languageErrorsAndBarriers = 0, worldLanguage = 0;
	private int metrics = -1;
	
	public int calculateDuMetrics(TwoOntologiesResults twoResults, 
								  SimpleTwoOntologiesResults simpleResults, 
								  OneOntologyResults oneResults,
								  AdueResults adueResults,
								  List<DealComponent> allTerms) {
		resetValues();
	
		domainContent = twoResults != null ? (twoResults.getListOfAdverseChangedComponents().size() 
											  + twoResults.getListOfDeletedComponents().size()) 
										   : (simpleResults != null ? simpleResults.getListOfDeletedElements().size() : 0) 
						+ (adueResults != null ? adueResults.getResultsOfToolTipHelper().size() : 0);
		languageErrorsAndBarriers = oneResults.getListOfGrammaticalErrors().size();
		domainSpecificity = oneResults.getListOfWrongParents().size();
		
		calculateDuMetrics(allTerms.size());
//		calculateDuMetricsPersonForm();
		
		return metrics;
	}
	
//	public int calculateDuMetricsPersonForm() {
//		float errorCoefficient = Math.round(WEIGHT_DOMAIN_CONTENT * 3 
//				 + WEIGHT_DOMAIN_SPECIFICITY * 3
//				 + WEIGHT_CONSISTENCY * consistency
//				 + WEIGHT_LANGUAGE_ERRORS_AND_BARRIERS * 2
//				 + WEIGHT_WORLD_LANGUAGE * worldLanguage);
//		
//		metrics = Math.round(Math.abs(100 * (1 - (errorCoefficient / 16))));
//		return metrics;
//	}
	
	private int calculateDuMetrics(int numberOfAllTerms) {
		float errorCoefficient = Math.round(WEIGHT_DOMAIN_CONTENT * domainContent 
				 + WEIGHT_DOMAIN_SPECIFICITY * domainSpecificity
				 + WEIGHT_CONSISTENCY * consistency
				 + WEIGHT_LANGUAGE_ERRORS_AND_BARRIERS * languageErrorsAndBarriers
				 + WEIGHT_WORLD_LANGUAGE * worldLanguage);
		
		metrics = Math.round(Math.abs(100 * (1 - (errorCoefficient / numberOfAllTerms))));
		return metrics;
	}
	
	public int getMetrics() {
		return metrics;
	}
	
	private void resetValues() {
		domainContent = 0;
		domainSpecificity = 0;
		consistency = 0;
		languageErrorsAndBarriers = 0;
		worldLanguage = 0;
		metrics = -1;
	}
	
	public String getMetricsInterpretation() {
		if(this.metrics <= 55) return "INSUFFICIENT";
		if(this.metrics < 70) return "SATISFACTORY";
		if(this.metrics < 80) return "GOOD";
		if(this.metrics < 90) return "VERY GOOD";
		return "EXCELLENT";
	}
}
