/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.evaluationLogic;

import gui.analyzer.tools.adue.evaluationLogic.results.ElementResultsWrapper;
import gui.analyzer.tools.adue.evaluationLogic.results.OneOntologyResults;
import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealElement;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

import sk.tuke.deal.ptf.ParentTermFinder;

/**
 * Abstraktna trieda obsahujuca metody na hodnotenie novych prvkov ontologie
 * (gramaticke chyby a zle definovane rodicovske pojmy). Zaroven predstavuje
 * background vorker, kedze tato akcia sa vykonava v samostantom vlakne.
 * 
 * @author Martin Zbuska
 */
public abstract class AbstractEvaluator extends SwingWorker<AbstractEvaluator, Integer> {

	/**
	 * Objekt vykonavajuci kontrolu gramatiky.
	 */
	private final DealSpellChecker spellChecker;
	
	/**
	 * Objekt vykonavajuci overovanie vhodnosti rodicovskych pojmov.
	 */
	private final ParentTermFinder parentFinder;
	
	/**
	 * Identifikator, ci maju byt hodnotene len nove prvky, alebo vsetky prvky novej ontlogie.
	 */
	protected final boolean fullCheck;

	/**
	 * Konstruktor.
	 * 
	 * @param fullCheck
	 *            identifikator ci sa maju kontrolovat vsetky prvky novej
	 *            ontologie, alebo len nove oproti povodnej ontologii.
	 */
	protected AbstractEvaluator(boolean fullCheck) {
		this.spellChecker = new DealSpellChecker();
		this.parentFinder = ParentTermFinder.getInstance();
		this.fullCheck = fullCheck;
	}

	/**
	 * Zisti ci dany text obsahuje gramaticke chyby. Ak ano navrhne mozne
	 * riesenia (5 prvych moznosti zo slovnika) Priklad:
	 * "I lke programming at schol" ->
	 * "I [lek, lake, leke, like, loke] programming at [chol, scheol, schola, schul, Chol]"
	 * 
	 * @param text
	 *            text na korekciu
	 * @return ak text obsahoval chyby tak dany text s nahradenimy chybami za
	 *         spravne moznosti (5), ak neobsahoval chyby tak null
	 */
	protected String getGrammarCorrection(String text) {
		String correction = null;
		if (text != null && !"".equals(text)) {
			spellChecker.clearMisspelledWords();
			List<String> misspelledWords = spellChecker.getMisspelledWords(text);
			if (!misspelledWords.isEmpty()) {
				correction = text;
				List<String> suggestions;
				for (String word : misspelledWords) {
					suggestions = spellChecker.getSuggestions(word);
					if (!suggestions.isEmpty()) {
						int maxSuggestions = suggestions.size();
						if (maxSuggestions > 5) {
							maxSuggestions = 5;
						}
						correction = correction.replaceAll(word, suggestions.subList(0, maxSuggestions).toString());
					} else {
						correction = correction.replaceAll(word, "[]");
					}
				}
			}
		}
		return correction;
	}

	/**
	 * Overi ci dany rodicovsky pojem je vhodny pre dane deti. Ak nie je navrhne
	 * ineho rodica.
	 * 
	 * @param parent
	 *            text predstavujuci rodica
	 * @param childs
	 *            zoznam slov reprezentujucich dcerske prvky daneho rodica
	 * @return Ak sa zisti vhodnejsi termin alebo aj viac pre rodica tak
	 *         navrhnute moznosti. Ak je termin pre rodica OK, tak null.
	 */
	protected String checkParent(String parent, List<String> childs) {
		parent = parent.toLowerCase();
		try {
			List<String> newParents = parentFinder.findParentTermsList((ArrayList<String>) childs);
			if (newParents != null) {
				if (parent != null) {
					if (newParents.contains(parent)) {
						return null;
					} else {
						return newParents.toString();
					}
				} else {
					return newParents.toString();
				}
			} else {
				if (parent != null) {
					return null;
				} else {
					return "!!You have to specify parent alone.";
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Zobrazenie vysledkov v konzole.
	 * 
	 * @param results
	 */
	protected void printOneResults(OneOntologyResults results) {
		System.out.println("Pocet prvkov s gramatickou chybou: " + results.getListOfGrammaticalErrors().size());
		for (ElementResultsWrapper wrap : results.getListOfGrammaticalErrors()) {
			if (wrap.getElement() instanceof DealElement) {
				DealElement result = (DealElement) wrap.getElement();
				System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getName() + "   " + result.getLabel() + "         " + wrap.getMisstake() + "   ->   " + wrap.getCorrection());
			} else {
				DealComponent result = (DealComponent) wrap.getElement();
				System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "         " + wrap.getMisstake() + "   ->   " + wrap.getCorrection());
			}
		}
		System.out.println("Pocet prvkov s nespravnym rodicom: " + results.getListOfWrongParents().size());
		for (ElementResultsWrapper wrap : results.getListOfWrongParents()) {
			if (wrap.getElement() instanceof DealElement) {
				DealElement result = (DealElement) wrap.getElement();
				System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getName() + "   " + result.getLabel() + "         " + wrap.getMisstake() + "   ->   " + wrap.getCorrection());
			} else {
				DealComponent result = (DealComponent) wrap.getElement();
				System.out.println("         " + result.getId() + "   " + result.getComponentClass() + "   " + result.getText() + "         " + wrap.getMisstake() + "   ->   " + wrap.getCorrection());
			}
		}
	}

	/**
	 * Metoda, ktora zabezpecuje spustenie hodnotenia v osobitnom vlakne.
	 */
	@Override
	protected abstract AbstractEvaluator doInBackground() throws Exception;

}
