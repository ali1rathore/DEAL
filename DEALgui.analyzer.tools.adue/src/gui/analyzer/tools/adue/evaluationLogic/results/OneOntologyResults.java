/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.evaluationLogic.results;

import gui.analyzer.tools.adue.owl.extracted.AbstractElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Trieda reprezentujuca vysledky hodnotenia ontologie vzhladom na gramaticke
 * chyby, a zle definovane rodicovske pojmy.
 * 
 * @author Martin Zbuska
 */
public class OneOntologyResults {

	/**
	 * zoznam komponentov ktore maju v texte gramaticku chybu, kazda polozka obsahuje element, chybu a korekciu
	 */
	private final List<ElementResultsWrapper> listOfGrammaticalErrors;

	/**
	 * zoznam komponentov ktore maju zle prideleneho rodica, kazda polozka obsahuje element, chybu a korekciu
	 */
	private final List<ElementResultsWrapper> listOfWrongParents;

	/**
	 * Konstruktor.
	 */
	public OneOntologyResults() {
		this.listOfWrongParents = new ArrayList<>();
		this.listOfGrammaticalErrors = new ArrayList<>();
	}

	/**
	 * @return the listOfGrammaticalErrors
	 */
	public List<ElementResultsWrapper> getListOfGrammaticalErrors() {
		return listOfGrammaticalErrors;
	}

	/**
	 * @return the listOfWrongParents
	 */
	public List<ElementResultsWrapper> getListOfWrongParents() {
		return listOfWrongParents;
	}

	/**
	 * Ziskanie polozky zo zonamu poloziek s gramatickou chybou.
	 * 
	 * @param element
	 *            objekt podla ktoreho ma byt najdena polozka v zozname.
	 * @return pozadovana polozka, ak sa v zozname s gramatickou chybou
	 *         nenachadzala tak null.
	 */
	public ElementResultsWrapper getElementWithGrammaticalErrorsByElement(AbstractElement element) {
		ElementResultsWrapper felement = new ElementResultsWrapper(element, null, null);
		if (listOfGrammaticalErrors.contains(felement)) {
			for (ElementResultsWrapper erw : listOfGrammaticalErrors) {
				if (erw.equals(felement)) {
					return erw;
				}
			}
		}
		return null;
	}

	/**
	 * Ziskanie polozky zo zonamu poloziek so zle definovanym rodicovskym
	 * pojmom.
	 * 
	 * @param element
	 *            element objekt podla ktoreho ma byt najdena polozka v zozname.
	 * @return pozadovana polozka, ak sa v zozname so zle definovanym
	 *         rodicovskym pojmom nenachadzala tak null.
	 */
	public ElementResultsWrapper geteElementWithWrongParentsByElement(AbstractElement element) {
		ElementResultsWrapper felement = new ElementResultsWrapper(element, null, null);
		if (listOfWrongParents.contains(felement)) {
			for (ElementResultsWrapper erw : listOfWrongParents) {
				if (erw.equals(felement)) {
					return erw;
				}
			}
		}
		return null;
	}

}
