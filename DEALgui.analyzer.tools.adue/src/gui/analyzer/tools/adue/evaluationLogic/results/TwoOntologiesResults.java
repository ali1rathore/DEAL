/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.evaluationLogic.results;

import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealComponentPair;

import java.util.ArrayList;
import java.util.List;

/**
 * Trieda reprezentujuca vysledky porovnavania dvoch ontologii, v pripade ze z
 * ontologii boli extrahovane komponenty.
 * 
 * @author Martin Zbuska
 */
public class TwoOntologiesResults {

	/**
	 * zoznam komponentov ktore ostali v novej tak ako v starej ontologii
	 */
	private List<DealComponent> listOfRetainedOldComponents;
	
	/**
	 * zoznam komponentov, ktore su nove a je potrebne ich ohodnotit
	 */
	private List<DealComponent> listOfNewComponents;
	
	/**
	 * zoznam komponentov, ktorych typ bol zmeneny (zle) napr z tlacidla na label stary, novy component
	 */
	private List<DealComponentPair> listOfAdverseChangedComponents;
	
	/**
	 * zoznam komponentov ktore boli oproti starej ontologii zmenene, ale dana zmena je povolena stary, novy component
	 */
	private List<DealComponentPair> listOfGoodChangedComponents;
	
	/**
	 * zoznam zmazanych komponentov, pri porovnavani dvoch ontologii by nova mala obsahovat co najviac starych
	 */
	private List<DealComponent> listOfDeletedComponents;

	/**
	 * Konstruktor.
	 */
	public TwoOntologiesResults() {
		this.listOfRetainedOldComponents = new ArrayList<>();
		this.listOfNewComponents = new ArrayList<>();
		this.listOfAdverseChangedComponents = new ArrayList<>();
		this.listOfGoodChangedComponents = new ArrayList<>();
		this.listOfDeletedComponents = new ArrayList<>();
	}

	/**
	 * Ziskanie objektu zo zoznamu parov dobre zmenenych komponentov podla
	 * objektu druheho komponentu (novy komponent na ktory bol predosli
	 * zmeneny).
	 * 
	 * @param component2
	 *            reprezentujuci hladanu dvojicu, ktorej novy komponent
	 *            predstavuje tento objekt.
	 * @return objekt reprezentujuci povodny komponent voci hladanemu
	 *         komponentu. Ak sa v zozname nenachadzal tak null.
	 */
	public DealComponent getGoodChangedComponent1ByComponent2(DealComponent component2) {
		for (DealComponentPair pair : listOfGoodChangedComponents) {
			if (pair.getComponent2().equals(component2)) {
				return pair.getComponent1();
			}
		}
		return null;
	}

	/**
	 * Ziskanie objektu zo zoznamu parov zle zmenenych komponentov podla objektu
	 * druheho komponentu (novy komponent na ktory bol predosli zmeneny).
	 * 
	 * @param component2
	 *            objekt reprezentujuci hladanu dvojicu, ktorej novy komponent
	 *            predstavuje tento objekt.
	 * @return objekt reprezentujuci povodny komponent voci hladanemu
	 *         komponentu. Ak sa v zozname nenachadzal tak null.
	 */
	public DealComponent getAdverseChangedComponent1ByComponent2(DealComponent component2) {
		for (DealComponentPair pair : listOfAdverseChangedComponents) {
			if (pair.getComponent2().equals(component2)) {
				return pair.getComponent1();
			}
		}
		return null;
	}

	/**
	 * @return the listOfRetainedOldComponents
	 */
	public List<DealComponent> getListOfRetainedOldComponents() {
		return listOfRetainedOldComponents;
	}

	/**
	 * @param listOfRetainedOldComponents
	 *            the listOfRetainedOldComponents to set
	 */
	public void setListOfRetainedOldComponents(List<DealComponent> listOfRetainedOldComponents) {
		this.listOfRetainedOldComponents = listOfRetainedOldComponents;
	}

	/**
	 * @return the listOfNewComponents
	 */
	public List<DealComponent> getListOfNewComponents() {
		return listOfNewComponents;
	}

	/**
	 * @param listOfNewComponents
	 *            the listOfNewComponents to set
	 */
	public void setListOfNewComponents(List<DealComponent> listOfNewComponents) {
		this.listOfNewComponents = listOfNewComponents;
	}

	/**
	 * @return the listOfAdverseChangedComponents
	 */
	public List<DealComponentPair> getListOfAdverseChangedComponents() {
		return listOfAdverseChangedComponents;
	}

	/**
	 * @param listOfAdverseChangedComponents
	 *            the listOfAdverseChangedComponents to set
	 */
	public void setListOfAdverseChangedComponents(List<DealComponentPair> listOfAdverseChangedComponents) {
		this.listOfAdverseChangedComponents = listOfAdverseChangedComponents;
	}

	/**
	 * @return the listOfGoodChangedComponents
	 */
	public List<DealComponentPair> getListOfGoodChangedComponents() {
		return listOfGoodChangedComponents;
	}

	/**
	 * @param listOfGoodChangedComponents
	 *            the listOfGoodChangedComponents to set
	 */
	public void setListOfGoodChangedComponents(List<DealComponentPair> listOfGoodChangedComponents) {
		this.listOfGoodChangedComponents = listOfGoodChangedComponents;
	}

	/**
	 * @return the listOfDeletedComponents
	 */
	public List<DealComponent> getListOfDeletedComponents() {
		return listOfDeletedComponents;
	}

	/**
	 * @param listOfDeletedComponents
	 *            the listOfDeletedComponents to set
	 */
	public void setListOfDeletedComponents(List<DealComponent> listOfDeletedComponents) {
		this.listOfDeletedComponents = listOfDeletedComponents;
	}

	@Override
	public String toString() {
		String string = "Pocet ponechanych prvkov: " + this.listOfRetainedOldComponents.size() + "\nPocet novych prvkov:      " + this.listOfNewComponents.size() + "\nPocet zmazanych prvkov:   " + this.listOfDeletedComponents.size() + "\nPocet zmenenych prvkov:   " + (this.listOfAdverseChangedComponents.size() + this.listOfGoodChangedComponents.size()) + "\n    Pocet nespravnych zmien:  " + this.listOfAdverseChangedComponents.size();
		return string;
	}

}
