/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.analyzer.tools.adue.evaluationLogic.results;

import gui.analyzer.tools.adue.owl.extracted.AbstractElement;

/**
 * Trieda reprezentujuca hodnoteny element ontologie spolu s informaciou o chybe
 * a navrhom na opravu.
 * 
 * @author Martin Zbuska
 */
public class ElementResultsWrapper {

	/**
	 * Element v kotorm bola najdena chyba.
	 */
	private final AbstractElement element;
	
	/**
	 * Najdena chyba.
	 */
	private final String misstake;
	
	/**
	 * Navrhnute mozne riesenia.
	 */
	private final String correction;

	/**
	 * Konstruktor.
	 * 
	 * @param element
	 *            objekt v ktorom bola najdena chyba.
	 * @param misstake
	 *            chyba ktora bola najdena.
	 * @param correction
	 *            navrh na opravu chyby.
	 */
	public ElementResultsWrapper(AbstractElement element, String misstake, String correction) {
		this.element = element;
		this.misstake = misstake;
		this.correction = correction;
	}

	/**
	 * @return the element
	 */
	public AbstractElement getElement() {
		return element;
	}

	/**
	 * @return the misstake
	 */
	public String getMisstake() {
		return misstake;
	}

	/**
	 * @return the correction
	 */
	public String getCorrection() {
		return correction;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof ElementResultsWrapper) {
			ElementResultsWrapper el = (ElementResultsWrapper) obj;
			return el.getElement().equals(element);
		}
		return false;
	}

}
