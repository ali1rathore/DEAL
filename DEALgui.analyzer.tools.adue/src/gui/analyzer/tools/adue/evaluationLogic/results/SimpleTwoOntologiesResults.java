/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.analyzer.tools.adue.evaluationLogic.results;

import gui.analyzer.tools.adue.owl.extracted.DealElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Trieda reprezentujuca vysledky porovnavania dvoch ontologii, v pripade ze z
 * ontologii boli extrahovane polozky ontologie a nie komponenty.
 * 
 * @author Martin Zbuska
 */
public class SimpleTwoOntologiesResults {

	/**
	 * Zoznam poloziek, ktore boli pouzite rovnako v novej ontologii ako boli pouzite v starej.
	 */
	private List<DealElement> listOfRetainedOldElements;
	
	/**
	 * Zoznam poloziek, ktore boli vytvorene v novej ontologii, a teda sa v starej nenachadzali.
	 */
	private List<DealElement> listOfNewElements;
	
	/**
	 * Zoznam poloziek zo starej ontologie, ktore boli v novej odstranene, teda neboli v novej pouzite.
	 */
	private List<DealElement> listOfDeletedElements;

	/**
	 * Konstruktor.
	 */
	public SimpleTwoOntologiesResults() {
		this.listOfRetainedOldElements = new ArrayList<>();
		this.listOfNewElements = new ArrayList<>();
		this.listOfDeletedElements = new ArrayList<>();
	}

	/**
	 * @return the listOfRetainedOldElements
	 */
	public List<DealElement> getListOfRetainedOldElements() {
		return listOfRetainedOldElements;
	}

	/**
	 * @param listOfRetainedOldElements
	 *            the listOfRetainedOldElements to set
	 */
	public void setListOfRetainedOldElements(List<DealElement> listOfRetainedOldElements) {
		this.listOfRetainedOldElements = listOfRetainedOldElements;
	}

	/**
	 * @return the listOfNewElements
	 */
	public List<DealElement> getListOfNewElements() {
		return listOfNewElements;
	}

	/**
	 * @param listOfNewElements
	 *            the listOfNewElements to set
	 */
	public void setListOfNewElements(List<DealElement> listOfNewElements) {
		this.listOfNewElements = listOfNewElements;
	}

	/**
	 * @return the listOfDeletedElements
	 */
	public List<DealElement> getListOfDeletedElements() {
		return listOfDeletedElements;
	}

	/**
	 * @param listOfDeletedElements
	 *            the listOfDeletedElements to set
	 */
	public void setListOfDeletedElements(List<DealElement> listOfDeletedElements) {
		this.listOfDeletedElements = listOfDeletedElements;
	}

}
