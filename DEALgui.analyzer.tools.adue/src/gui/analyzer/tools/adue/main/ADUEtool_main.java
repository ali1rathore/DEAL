package gui.analyzer.tools.adue.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.SwingWorker.StateValue;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import gui.analyzer.tools.adue.evaluationLogic.DealSpellChecker;
import gui.analyzer.tools.adue.evaluationLogic.DuMetricsCalculator;
import gui.analyzer.tools.adue.evaluationLogic.OntologyEvaluator;
import gui.analyzer.tools.adue.evaluationLogic.SimpleOntologyEvaluator;
import gui.analyzer.tools.adue.evaluationLogic.results.ElementResultsWrapper;
import gui.analyzer.tools.adue.evaluationLogic.results.OneOntologyResults;
import gui.analyzer.tools.adue.evaluationLogic.results.SimpleTwoOntologiesResults;
import gui.analyzer.tools.adue.evaluationLogic.results.TwoOntologiesResults;
import gui.analyzer.tools.adue.owl.extracted.AbstractElement;
import gui.analyzer.tools.adue.owl.extracted.DealComponent;
import gui.analyzer.tools.adue.owl.extracted.DealComponentPair;
import gui.analyzer.tools.adue.owl.extracted.DealElement;
import gui.analyzer.tools.adue.ui.observable.AdueResults;
import gui.analyzer.tools.adue.ui.tools.OntologyListRenderer;
import gui.analyzer.tools.adue.ui.tools.RowHeaderTable;

/**
 * Trieda reprezentujuca pouzivatelske rozhranie automatiozvaneho hodnotenia
 * domenovej pouzitelnosti prostrednictvom ontologii.
 * 
 * @author Martin Zbuska
 */

@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
public class ADUEtool_main extends javax.swing.JFrame {
	private static final long serialVersionUID = -4911275626327667529L;

	/**
	 * Cesta k png suborom pouzitych v legende tohto nastroja.
	 */
    private final String LEGEND_IMAGES_PATH = "/gui/analyzer/tools/adue/ui/icons/";
    
    /**
     * Cesta k suboru starej ontologie.
     */
    private String oldOntologyPath;
    
    /**
     * Cesta k suboru novej ontologie.
     */
    private String newOntologyPath;

    public static final String TITLE_LOAD_ORIGINAL_ONTOLOGY = "Load original ontology";
    public static final String TITLE_LOAD_NEW_ONTOLOGY = "Load new ontolgoy";
    public static final String TITLE_LOAD_GRAMMAR_DICTIONARY = "Load grammar dictionary";

    /**
     * Objekt reprezentujuci prvy sposob extrakcie/hodnotenia ontologie (na zaklade komponentov).
     */
    private OntologyEvaluator ontologyEvaluator;
    
    /**
     * Objekt reprezentujuci druhy sposob extrakcie/hodnotenia ontologie (na zaklade poloziek tak ako su v ontologii).
     */
    private SimpleOntologyEvaluator simpleOntologyEvaluator;

    /**
     * Objekt reprezentujuci vysledky porovnania ontologii prvym sposobom (na zaklade komponentov).
     */
    private TwoOntologiesResults twoOntologiesResults;
    
    /**
     * Objekt reprezentujuci vysledky porovnania ontologie druhym sposobom (na zaklade poloziek tak ako su v ontologii).
     */
    private SimpleTwoOntologiesResults simpleTwoOntologiesResults;
    
    /**
     * Objekt reprezentujuci vysledky kontroly gramatiky a rodicovskych pojmov poloziek novej ontologie.
     */
    private OneOntologyResults oneOntologyResults;
    
    /**
     * To be able to use the number of missing tooltips as domain usability issues and to calculate the resulting domain usability score.
     */
    private AdueResults adueResults;
    public void setAdueResults(AdueResults adueResults) {
		this.adueResults = adueResults;
	}
    
    /**
     * Calculates the domain usability score using our domain usability metrics.
     */
    private DuMetricsCalculator duMetricsCalculator;

    private final String GUI_ELEMENTS_EVALUATION = "GUI elements evaluation";
    private final String SIMPLE__ONTOLOGY_EVALUATION = "Simple ontology evaluation";

    private final String[] COLUMN_NAMES_LONG_EVALUATOR = {
        "OLD",
        "NEW",
        "DELETED",
        "CHANGED",};
    private final String[] COLUMN_NAMES_SIMPLE_EVALUATOR = {
        "OLD",
        "NEW",
        "DELETED",
    };
    private final String[] COLUMN_NAMES_OF_SUGGESTIONS = {
        "GRAMMAR",
        "PARENT NAME"
    };

    private long startTime;
    private long endTime;
    
    private void initDuScorePanel() {
    	duScorePanel.setLayout(new BoxLayout(duScorePanel, BoxLayout.PAGE_AXIS));
    	duScoreValueLabel = new JLabel("-- %");
    	//100 - 90%Excellent90 - 80%Very good80 - 70%Good70 - 55%Satisfactoryless than 55%Insufficient
    	duScoreValueLabel.setFont(new Font(duScoreValueLabel.getName(), Font.BOLD, 18));
    	duScoreValueLabel.setHorizontalAlignment(JLabel.RIGHT);
    	duScoreValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    	duScorePanel.add(duScoreValueLabel);
    }

	/**
	 * Inicializacia a nastavenie stavoveho riadka pouzivatelskeho rozhrania.
	 */
    private void initStatusPanel(){
        infoPanel.setLayout(new GridLayout(0, 6));
        statusRetainedLabel = new JLabel("0 retained");
        statusRetainedLabel.setHorizontalAlignment(JLabel.CENTER);
        statusNewLabel = new JLabel("0 new");
        statusNewLabel.setHorizontalAlignment(JLabel.CENTER);
        statusDeletedLabel = new JLabel("0 deleted");
        statusDeletedLabel.setHorizontalAlignment(JLabel.CENTER);
        statusChangedLabel = new JLabel("0 changed");
        statusChangedLabel.setHorizontalAlignment(JLabel.CENTER);
        statusGrammarLabel = new JLabel("0 grammar error");
        statusGrammarLabel.setHorizontalAlignment(JLabel.CENTER);
        statusParentLabel = new JLabel("0 wrong parent");
        statusParentLabel.setHorizontalAlignment(JLabel.CENTER);
        infoPanel.add(statusRetainedLabel);
        infoPanel.add(statusNewLabel);
        infoPanel.add(statusDeletedLabel);
        infoPanel.add(statusChangedLabel);
        infoPanel.add(statusGrammarLabel);
        infoPanel.add(statusParentLabel);
    }
    
	/**
	 * Inicializacia a nastavenie legendy pouzivatelskeho rozhrania.
	 */
    private void initLegend(){
        legendPanel.setLayout(new GridLayout(0, 3));
        JLabel grammarLabel = new JLabel("Element contains grammar mistakes.");
        grammarLabel.setIcon(new ImageIcon(getClass().getResource(LEGEND_IMAGES_PATH + "grammar_error.png")));
        grammarLabel.setHorizontalAlignment(JLabel.CENTER);
        legendPanel.add(grammarLabel);
        JLabel parentLabel = new JLabel("Wrong defined name for parent.");
        parentLabel.setIcon(new ImageIcon(getClass().getResource(LEGEND_IMAGES_PATH + "parent_error.png")));
        parentLabel.setHorizontalAlignment(JLabel.CENTER);
        legendPanel.add(parentLabel);
        JLabel changeLabel = new JLabel("Wrong changed element.");        
        changeLabel.setIcon(new ImageIcon(getClass().getResource(LEGEND_IMAGES_PATH + "change_error.png")));
        changeLabel.setHorizontalAlignment(JLabel.CENTER);
        legendPanel.add(changeLabel);
    }
    
    /**
     * Konstruktor.
     * @param testedFile subor reprezentujuci testovanu ontologiu.
     */
	public ADUEtool_main(File testedFile) {
        initComponents();

        initStatusPanel();
        initDuScorePanel();
        initLegend();

        String dictName = (new File(DealSpellChecker.getDictionaryPath())).getName();
        dictionaryNameLabel.setText(dictName);
        evaluationTypeComboBox.setModel(new DefaultComboBoxModel(new String[]{GUI_ELEMENTS_EVALUATION, SIMPLE__ONTOLOGY_EVALUATION}));

        DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        table.setRowHeight(20);
        table.setEnabled(false);
        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.getTableHeader().setReorderingAllowed(false);
        table.getTableHeader().setResizingAllowed(false);

        DefaultTableCellRenderer suggestionsTableRenderer = (DefaultTableCellRenderer) suggestionsTable.getTableHeader().getDefaultRenderer();
        suggestionsTableRenderer.setHorizontalAlignment(JLabel.CENTER);
        suggestionsTable.setRowHeight(20);
        suggestionsTable.setEnabled(false);
        suggestionsTable.setShowHorizontalLines(true);
        suggestionsTable.setShowVerticalLines(true);
        suggestionsTable.getTableHeader().setReorderingAllowed(false);
        suggestionsTable.getTableHeader().setResizingAllowed(false);

        newOntologyNameLabel.setText(testedFile.getName());
        this.newOntologyPath = testedFile.getAbsolutePath();
        fullCheckCheckBox.setEnabled(false);
        
        duMetricsCalculator = new DuMetricsCalculator();
    }

    /**
     * Konstruktor.
     */
	public ADUEtool_main() {
        initComponents();

        initStatusPanel();
        initLegend();

        String dictName = (new File(DealSpellChecker.getDictionaryPath())).getName();
        dictionaryNameLabel.setText(dictName);
        evaluationTypeComboBox.setModel(new DefaultComboBoxModel(new String[]{GUI_ELEMENTS_EVALUATION, SIMPLE__ONTOLOGY_EVALUATION}));

        DefaultTableCellRenderer renderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        table.setRowHeight(20);
        table.setEnabled(false);
        table.setShowHorizontalLines(true);
        table.setShowVerticalLines(true);
        table.getTableHeader().setReorderingAllowed(false);
        table.getTableHeader().setResizingAllowed(false);

        DefaultTableCellRenderer suggestionsTableRenderer = (DefaultTableCellRenderer) suggestionsTable.getTableHeader().getDefaultRenderer();
        suggestionsTableRenderer.setHorizontalAlignment(JLabel.CENTER);
        suggestionsTable.setRowHeight(20);
        suggestionsTable.setEnabled(false);
        suggestionsTable.setShowHorizontalLines(true);
        suggestionsTable.setShowVerticalLines(true);
        suggestionsTable.getTableHeader().setReorderingAllowed(false);
        suggestionsTable.getTableHeader().setResizingAllowed(false);
        evaluateOntologyButton.setEnabled(false);
        fullCheckCheckBox.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        loadNewOntologyButton = new javax.swing.JButton();
        setDictionaryButton = new javax.swing.JButton();
        loadOriginalOntologyButton = new javax.swing.JButton();
        newOntologyNameLabel = new javax.swing.JLabel();
        dictionaryNameLabel = new javax.swing.JLabel();
        label2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        originalOntologyNameLabel = new javax.swing.JLabel();
        label1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        evaluateOntologyButton = new javax.swing.JButton();
        fullCheckCheckBox = new javax.swing.JCheckBox();
        progressBar = new javax.swing.JProgressBar();
        evaluationTypeComboBox = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        parentsCheckBox = new javax.swing.JCheckBox();
        grammarCheckBox = new javax.swing.JCheckBox();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        changedCheckBox = new javax.swing.JCheckBox();
        newCheckBox = new javax.swing.JCheckBox();
        retainedCheckBox = new javax.swing.JCheckBox();
        deletedCheckBox = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        suggestionsTableScrollPane = new javax.swing.JScrollPane();
        suggestionsTable = new javax.swing.JTable();
        tableScrollPane = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        leftScrollPane = new javax.swing.JScrollPane();
        originalOntologyList = new javax.swing.JList();
        rightScrollPane = new javax.swing.JScrollPane();
        newOntologyList = new javax.swing.JList();
        infoPanel = new javax.swing.JPanel();
        duScorePanel = new javax.swing.JPanel();
        legendPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ADUE");
        setMinimumSize(new java.awt.Dimension(713, 968));
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Loading files", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        loadNewOntologyButton.setText("Browse");
        loadNewOntologyButton.setToolTipText("Load new ontology file.");
        loadNewOntologyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadNewOntologyButtonActionPerformed(evt);
            }
        });

        setDictionaryButton.setText("Choose Dictionary");
        setDictionaryButton.setToolTipText("Choose dictionary file for spell check.");
        setDictionaryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setDictionaryButtonActionPerformed(evt);
            }
        });

        loadOriginalOntologyButton.setText("Browse");
        loadOriginalOntologyButton.setToolTipText("Load original ontology file.");
        loadOriginalOntologyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadOriginalOntologyButtonActionPerformed(evt);
            }
        });

        newOntologyNameLabel.setMaximumSize(new java.awt.Dimension(221, 14));
        newOntologyNameLabel.setMinimumSize(new java.awt.Dimension(221, 14));
        newOntologyNameLabel.setPreferredSize(new java.awt.Dimension(221, 14));

        dictionaryNameLabel.setMaximumSize(new java.awt.Dimension(130, 14));
        dictionaryNameLabel.setMinimumSize(new java.awt.Dimension(130, 14));
        dictionaryNameLabel.setPreferredSize(new java.awt.Dimension(130, 14));

        label2.setText("New ontology:");
        label2.setMaximumSize(new java.awt.Dimension(285, 14));

        jLabel1.setText("Dictionary:");

        originalOntologyNameLabel.setMaximumSize(new java.awt.Dimension(221, 14));
        originalOntologyNameLabel.setMinimumSize(new java.awt.Dimension(221, 14));
        originalOntologyNameLabel.setPreferredSize(new java.awt.Dimension(221, 14));

        label1.setText("Original ontology:");
        label1.setMaximumSize(new java.awt.Dimension(285, 14));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(loadOriginalOntologyButton)
                    .addComponent(loadNewOntologyButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(newOntologyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(originalOntologyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(setDictionaryButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dictionaryNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(loadOriginalOntologyButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(dictionaryNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(originalOntologyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(loadNewOntologyButton)
                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(setDictionaryButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(newOntologyNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Domain usability evaluating", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        evaluateOntologyButton.setText("Start usability evaluation");
        evaluateOntologyButton.setToolTipText("Start evaluating selected ontologies.");
        evaluateOntologyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                evaluateOntologyButtonActionPerformed(evt);
            }
        });

        fullCheckCheckBox.setText("Full error check");
        fullCheckCheckBox.setToolTipText("Turn on/off checking mistakes in all elements in new ontology.");

        progressBar.setStringPainted(true);

        evaluationTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "evaluation types" }));
        evaluationTypeComboBox.setToolTipText("Choose type of evaluation.");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(evaluateOntologyButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(evaluationTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fullCheckCheckBox)
                .addGap(33, 33, 33)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(evaluateOntologyButton)
                        .addComponent(evaluationTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(fullCheckCheckBox))
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Display options", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        jLayeredPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Errors in new ontology", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N

        parentsCheckBox.setSelected(true);
        parentsCheckBox.setText("Parents");
        parentsCheckBox.setToolTipText("Show / Hide elements in the new ontology with wrong defined parents.");
        parentsCheckBox.setEnabled(false);
        parentsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parentsCheckBoxActionPerformed(evt);
            }
        });

        grammarCheckBox.setSelected(true);
        grammarCheckBox.setText("Grammatical");
        grammarCheckBox.setToolTipText("Show / Hide elements in the new ontology with grammatical mistake.");
        grammarCheckBox.setEnabled(false);
        grammarCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grammarCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(grammarCheckBox)
                .addGap(18, 18, 18)
                .addComponent(parentsCheckBox)
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(grammarCheckBox)
                    .addComponent(parentsCheckBox))
                .addContainerGap())
        );
        jLayeredPane2.setLayer(parentsCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(grammarCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLayeredPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Element's status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N

        changedCheckBox.setSelected(true);
        changedCheckBox.setText("Changed");
        changedCheckBox.setToolTipText("Show / Hide changed elements in the new ontology compared to old ontology.");
        changedCheckBox.setEnabled(false);
        changedCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changedCheckBoxActionPerformed(evt);
            }
        });

        newCheckBox.setSelected(true);
        newCheckBox.setText("New");
        newCheckBox.setToolTipText("Show / Hide new elements in the new ontology compared to old ontology.");
        newCheckBox.setEnabled(false);
        newCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newCheckBoxActionPerformed(evt);
            }
        });

        retainedCheckBox.setSelected(true);
        retainedCheckBox.setText("Retained");
        retainedCheckBox.setToolTipText("Show / Hide retained elements from original ontology in new ontology.");
        retainedCheckBox.setEnabled(false);
        retainedCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retainedCheckBoxActionPerformed(evt);
            }
        });

        deletedCheckBox.setSelected(true);
        deletedCheckBox.setText("Deleted");
        deletedCheckBox.setToolTipText("Show / Hide deleted elements from old ontology in the new ontology.");
        deletedCheckBox.setEnabled(false);
        deletedCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletedCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(retainedCheckBox)
                .addGap(18, 18, 18)
                .addComponent(newCheckBox)
                .addGap(18, 18, 18)
                .addComponent(deletedCheckBox)
                .addGap(18, 18, 18)
                .addComponent(changedCheckBox)
                .addContainerGap(17, Short.MAX_VALUE))
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(retainedCheckBox)
                    .addComponent(newCheckBox)
                    .addComponent(changedCheckBox)
                    .addComponent(deletedCheckBox))
                .addContainerGap())
        );
        jLayeredPane1.setLayer(changedCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(newCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(retainedCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(deletedCheckBox, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Results", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        suggestionsTableScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Table of suggestions for", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N

        suggestionsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        suggestionsTableScrollPane.setViewportView(suggestionsTable);

        tableScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "Talbe of element's status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N
        tableScrollPane.setPreferredSize(new java.awt.Dimension(452, 399));

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        tableScrollPane.setViewportView(table);

        leftScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Elements in original ontology", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N

        originalOntologyList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        originalOntologyList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                originalOntologyListMouseClicked(evt);
            }
        });
        leftScrollPane.setViewportView(originalOntologyList);

        rightScrollPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Elements in new ontology", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 11))); // NOI18N

        newOntologyList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        newOntologyList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                newOntologyListMouseClicked(evt);
            }
        });
        rightScrollPane.setViewportView(newOntologyList);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(tableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(suggestionsTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(leftScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addComponent(rightScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(leftScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rightScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(suggestionsTableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tableScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        infoPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.lightGray, java.awt.Color.gray, java.awt.Color.lightGray, java.awt.Color.gray));

        javax.swing.GroupLayout infoPanelLayout = new javax.swing.GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        infoPanelLayout.setVerticalGroup(
            infoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        
        duScorePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Domain Usability Score", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        duScorePanel.setPreferredSize(new java.awt.Dimension(4, 38));
        
        javax.swing.GroupLayout duScorePanelLayout = new javax.swing.GroupLayout(duScorePanel);
        duScorePanel.setLayout(duScorePanelLayout);
        duScorePanelLayout.setHorizontalGroup(
    		duScorePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        duScorePanelLayout.setVerticalGroup(
    		duScorePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 27, Short.MAX_VALUE)
        );

        legendPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(2, 0, 0, 0, new java.awt.Color(0, 0, 0)), "Legend", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        legendPanel.setPreferredSize(new java.awt.Dimension(4, 22));

        javax.swing.GroupLayout legendPanelLayout = new javax.swing.GroupLayout(legendPanel);
        legendPanel.setLayout(legendPanelLayout);
        legendPanelLayout.setHorizontalGroup(
            legendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        legendPanelLayout.setVerticalGroup(
            legendPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 27, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(duScorePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(legendPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 693, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(infoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(duScorePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(legendPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Nastavenie cesty k suboru reprezentujuceho originalnu ontologiu.
     * @param evt
     */
    private void loadOriginalOntologyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadOriginalOntologyButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooser.setDialogTitle(TITLE_LOAD_ORIGINAL_ONTOLOGY);
		if (oldOntologyPath != null) {
			chooser.setCurrentDirectory(new File(oldOntologyPath));
		}
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Ontology files (*.owl)", "owl"));

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            this.oldOntologyPath = chooser.getSelectedFile().getAbsolutePath();
            this.originalOntologyNameLabel.setText(chooser.getSelectedFile().getName());
            this.originalOntologyNameLabel.setToolTipText(chooser.getSelectedFile().getName());
            this.fullCheckCheckBox.setEnabled(true);
        }
    }//GEN-LAST:event_loadOriginalOntologyButtonActionPerformed

    /**
     * Spustenie hodnotenia nacitanych ontologii.
     * @param evt
     */
    private void evaluateOntologyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_evaluateOntologyButtonActionPerformed
        if (evaluationTypeComboBox.getSelectedItem().equals(GUI_ELEMENTS_EVALUATION)) {
            startEvaluatingOnBackground(ontologyEvaluator);
        } else if (evaluationTypeComboBox.getSelectedItem().equals(SIMPLE__ONTOLOGY_EVALUATION)) {
            startEvaluatingOnBackground(simpleOntologyEvaluator);
        }
    }//GEN-LAST:event_evaluateOntologyButtonActionPerformed
    
    /**
     * Nastavenie cesty k suboru reprezentujuceho gramaticky slovnik.
     * @param evt
     */
    private void setDictionaryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setDictionaryButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooser.setDialogTitle(TITLE_LOAD_GRAMMAR_DICTIONARY);
        chooser.setCurrentDirectory(new File(DealSpellChecker.getDictionaryPath()));
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Dictionary files (*.txt)", "txt"));

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            DealSpellChecker.setDictionary(chooser.getSelectedFile().getAbsolutePath());
            this.dictionaryNameLabel.setText(chooser.getSelectedFile().getName());
            this.dictionaryNameLabel.setToolTipText(chooser.getSelectedFile().getName());
        }
    }//GEN-LAST:event_setDictionaryButtonActionPerformed

	/**
	 * Akcia vykonavajuca najdenie a oznacenie paroveho prvku zoznamu v zozname
	 * napravo (zoznam testovanej ontologie), po kliknuti na lavy zoznam. Metoda
	 * vola metodu na naplnenie tabulky s informaciami o oznacenych polozkach
	 * zoznamov.
	 * 
	 * @param evt
	 */
	private void originalOntologyListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_originalOntologyListMouseClicked
        if (originalOntologyList.getModel().getSize() > 0 && (twoOntologiesResults != null || simpleTwoOntologiesResults != null)) {
            int selectedIndex = originalOntologyList.getSelectedIndex();
            ListModel newOntologyModel = newOntologyList.getModel();

            if (twoOntologiesResults != null) {
                DealComponent leftComponent = (DealComponent) originalOntologyList.getModel().getElementAt(selectedIndex);
                // Ak je element z laveho listu v zozname deletedComponents znamena ze v pravo nebude,
                // pretoze nebol obsiahnuty v novej ontologii.
                // Pozor ale moze byt medzi zmenenymi - toto plati len pri long evaluator, simple evaluator nema zmenene prvky
                if (twoOntologiesResults.getListOfDeletedComponents().contains(leftComponent)) {
                    newOntologyList.clearSelection();
                    populateTable(leftComponent, null);
                } else if (twoOntologiesResults.getListOfRetainedOldComponents().contains(leftComponent)) {
                    DealComponent rightComponent = null;
                    //najdi komponent v pravo
                    for (int i = 0; i < newOntologyModel.getSize(); i++) {
                        DealComponent dcmp = (DealComponent) newOntologyModel.getElementAt(i);
                        if (leftComponent.equals(dcmp)) {
                            newOntologyList.setSelectedIndex(i);
                            rightComponent = dcmp;
                            break;
                        }
                    }
                    populateTable(leftComponent, rightComponent);
                } else {
                    DealComponent rightComponent = null;
                    //najdi zmeneny komponent v pravo
                    List<DealComponentPair> listOfChangedComponents = new ArrayList<>(twoOntologiesResults.getListOfGoodChangedComponents());
                    listOfChangedComponents.addAll(new ArrayList<>(twoOntologiesResults.getListOfAdverseChangedComponents()));
                    for (int i = 0; i < newOntologyModel.getSize(); i++) {
                        DealComponent dcmp = (DealComponent) newOntologyModel.getElementAt(i);
                        if (listOfChangedComponents.contains(new DealComponentPair(leftComponent, dcmp))) {
                            newOntologyList.setSelectedIndex(i);
                            rightComponent = dcmp;
                            break;
                        }
                    }
                    populateTable(leftComponent, rightComponent);
                }
            } else if (simpleTwoOntologiesResults != null) {
                DealElement leftElement = (DealElement) originalOntologyList.getModel().getElementAt(selectedIndex);
                // Ak je element z laveho listu v zozname deletedElement znamena ze v pravo nebude,
                // pretoze nebol obsiahnuty v novej ontologii.
                if (simpleTwoOntologiesResults.getListOfDeletedElements().contains(leftElement)) {
                    newOntologyList.clearSelection();
                    populateTable(leftElement, null);
                } else {
                    DealElement rightElement = null;
                    //najdi element v pravo
                    for (int i = 0; i < newOntologyModel.getSize(); i++) {
                        DealElement delm = (DealElement) newOntologyModel.getElementAt(i);
                        if (leftElement.equals(delm)) {
                            newOntologyList.setSelectedIndex(i);
                            rightElement = delm;
                            break;
                        }
                    }
                    populateTable(leftElement, rightElement);
                }
            }
        }
    }//GEN-LAST:event_originalOntologyListMouseClicked

    /**
     * Nastavenie cesty k suboru reprezentujuceho testovanu ontologiu.
     * @param evt
     */
    private void loadNewOntologyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadNewOntologyButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setMultiSelectionEnabled(false);
        chooser.setDialogTitle(TITLE_LOAD_NEW_ONTOLOGY);
		if (newOntologyPath != null) {
			chooser.setCurrentDirectory(new File(newOntologyPath));
		}
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(new FileNameExtensionFilter("Ontology files (*.owl)", "owl"));

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            this.newOntologyPath = chooser.getSelectedFile().getAbsolutePath();
            this.newOntologyNameLabel.setText(chooser.getSelectedFile().getName());
            this.newOntologyNameLabel.setToolTipText(chooser.getSelectedFile().getName());
            evaluateOntologyButton.setEnabled(true);
        }
    }//GEN-LAST:event_loadNewOntologyButtonActionPerformed

	/**
	 * Akcia vykonavajuca najdenie a oznacenie paroveho prvku zoznamu v zozname
	 * nalavo (zoznam originalnej ontologie), po kliknuti na pravy zoznam.
	 * Metoda vola metodu na naplnenie tabulky s informaciami o oznacenych
	 * polozkach.
	 * 
	 * @param evt
	 */
    private void newOntologyListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newOntologyListMouseClicked
        if (newOntologyList.getModel().getSize() > 0) {
            int selectedIndex = newOntologyList.getSelectedIndex();
            ListModel originalOntologyModel = originalOntologyList.getModel();

            if (twoOntologiesResults != null) {
                DealComponent rightComponent = (DealComponent) newOntologyList.getModel().getElementAt(selectedIndex);
                //Ak je tento element z praveho listu v zozname newComponents znamena ze v lavo nebude
                // pretoze nebol obsiahnuty v starej ontologii.
                // Pozor moze byt medzi zmenenymi - toto plati len pri long evaluator, simpleEvaluator zmenene prvky
                if (twoOntologiesResults.getListOfNewComponents().contains(rightComponent)) {
                    originalOntologyList.clearSelection();
                    populateTable(null, rightComponent);
                } else if (twoOntologiesResults.getListOfRetainedOldComponents().contains(rightComponent)) {
                    DealComponent leftComponent = rightComponent;
                    //najdi komponent v lavo
                    for (int i = 0; i < originalOntologyModel.getSize(); i++) {
                        DealComponent dcmp = (DealComponent) originalOntologyModel.getElementAt(i);
                        if (rightComponent.equals(dcmp)) {
                            originalOntologyList.setSelectedIndex(i);                            
                            break;
                        }
                    }
                    populateTable(leftComponent, rightComponent);
                } else {
                    DealComponent leftComponent = twoOntologiesResults.getGoodChangedComponent1ByComponent2(rightComponent);
                    if(leftComponent == null){
                        leftComponent = twoOntologiesResults.getAdverseChangedComponent1ByComponent2(rightComponent);
                    }
                    //najdi zmeneny komponent v lavo                    
                    List<DealComponentPair> listOfChangedComponents = new ArrayList<>(twoOntologiesResults.getListOfGoodChangedComponents());
                    listOfChangedComponents.addAll(new ArrayList<>(twoOntologiesResults.getListOfAdverseChangedComponents()));
                    for (int i = 0; i < originalOntologyModel.getSize(); i++) {
                        DealComponent dcmp = (DealComponent) originalOntologyModel.getElementAt(i);
                        if (listOfChangedComponents.contains(new DealComponentPair(dcmp, rightComponent))) {
                            originalOntologyList.setSelectedIndex(i);
                            break;
                        }
                    }
                    populateTable(leftComponent, rightComponent);
                }
            } else if (simpleTwoOntologiesResults != null) {
                DealElement rightElement = (DealElement) newOntologyList.getModel().getElementAt(selectedIndex);
                // Ak je element z praveho listu v zozname newElement znamena ze v lavo nebude,
                // pretoze nebol obsiahnuty v starej ontologii
                if (simpleTwoOntologiesResults.getListOfNewElements().contains(rightElement)) {
                    originalOntologyList.clearSelection();
                    populateTable(null, rightElement);
                } else {
                    DealElement leftElement = null;
                    if(simpleTwoOntologiesResults.getListOfRetainedOldElements().contains(rightElement)){
                        leftElement = rightElement;
                    }
                    //najdi element v lavo
                    for (int i = 0; i < originalOntologyModel.getSize(); i++) {
                        DealElement delm = (DealElement) originalOntologyModel.getElementAt(i);
                        if (rightElement.equals(delm)) {
                            originalOntologyList.setSelectedIndex(i);
                            break;
                        }
                    }
                    populateTable(leftElement, rightElement);
                }
            } else {
                // Tu je len nova ontologia, takze sa vypisuju len vysledky z OneOntologyResults
                AbstractElement element = (AbstractElement) newOntologyList.getModel().getElementAt(selectedIndex);
                populateTable(null, element);
            }
        }
    }//GEN-LAST:event_newOntologyListMouseClicked

	/**
	 * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo retainedCheckBox.
	 * 
	 * @param evt
	 */
    private void retainedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retainedCheckBoxActionPerformed
        someCheckBoxChanged();
    }//GEN-LAST:event_retainedCheckBoxActionPerformed

	/**
	 * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo newCheckBox.
	 * 
	 * @param evt
	 */
    private void newCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newCheckBoxActionPerformed
        if(newCheckBox.isSelected()){
            grammarCheckBox.setSelected(true);
            parentsCheckBox.setSelected(true);
        } else {
            grammarCheckBox.setSelected(false);
            parentsCheckBox.setSelected(false);
        }
        someCheckBoxChanged();
    }//GEN-LAST:event_newCheckBoxActionPerformed

    /**
     * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo deletedCheckBox.
     * @param evt
     */
    private void deletedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletedCheckBoxActionPerformed
        someCheckBoxChanged();
    }//GEN-LAST:event_deletedCheckBoxActionPerformed

    /**
     * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo changedCheckBox.
     * @param evt
     */
    private void changedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changedCheckBoxActionPerformed
        someCheckBoxChanged();
    }//GEN-LAST:event_changedCheckBoxActionPerformed

    /**
     * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo grammarCheckBox.
     * @param evt
     */
    private void grammarCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grammarCheckBoxActionPerformed
        if(!grammarCheckBox.isSelected()){
            newCheckBox.setSelected(false);
        }
        someCheckBoxChanged();
    }//GEN-LAST:event_grammarCheckBoxActionPerformed

    /**
     * Akcia zabezpecujuca vypisanie aktualnych poloziek zoznamov, ak bolo
	 * kliknute na tlacidlo parentsCheckBox.
     * @param evt
     */
    private void parentsCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parentsCheckBoxActionPerformed
        if(!parentsCheckBox.isSelected()){
            newCheckBox.setSelected(false);
        }
        someCheckBoxChanged();
    }//GEN-LAST:event_parentsCheckBoxActionPerformed

	/**
	 * Main metoda, na spustenie aplikacie mimo nastroja DEAL. Pre samostatne
	 * fungovanie aplikacie.
	 * 
	 * @param args
	 *            the command line arguments
	 */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ADUEtool_main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ADUEtool_main().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox changedCheckBox;
    private javax.swing.JCheckBox deletedCheckBox;
    private javax.swing.JLabel dictionaryNameLabel;
    private javax.swing.JButton evaluateOntologyButton;
    private javax.swing.JComboBox<String[]> evaluationTypeComboBox;
    private javax.swing.JCheckBox fullCheckCheckBox;
    private javax.swing.JCheckBox grammarCheckBox;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JPanel duScorePanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JScrollPane leftScrollPane;
    private javax.swing.JPanel legendPanel;
    private javax.swing.JButton loadNewOntologyButton;
    private javax.swing.JButton loadOriginalOntologyButton;
    private javax.swing.JCheckBox newCheckBox;
    private javax.swing.JList newOntologyList;
    private javax.swing.JLabel newOntologyNameLabel;
    private javax.swing.JList originalOntologyList;
    private javax.swing.JLabel originalOntologyNameLabel;
    private javax.swing.JCheckBox parentsCheckBox;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JCheckBox retainedCheckBox;
    private javax.swing.JScrollPane rightScrollPane;
    private javax.swing.JButton setDictionaryButton;
    private javax.swing.JTable suggestionsTable;
    private javax.swing.JScrollPane suggestionsTableScrollPane;
    private javax.swing.JTable table;
    private javax.swing.JScrollPane tableScrollPane;
    // End of variables declaration//GEN-END:variables

    private javax.swing.JLabel statusRetainedLabel;
    private javax.swing.JLabel statusNewLabel;
    private javax.swing.JLabel statusDeletedLabel;
    private javax.swing.JLabel statusChangedLabel;
    private javax.swing.JLabel statusGrammarLabel;
    private javax.swing.JLabel statusParentLabel;
    private javax.swing.JLabel duScoreValueLabel;
    
	/**
	 * Zobrazenie vysledkov hodnotenia v zoznamoch, podla zvolenych parametrov
	 * (zaskrtnute checkboxy).
	 * 
	 * @param oe
	 *            objekt reprezentujuci akym sposobom boli ontologie hodnotene.
	 *            Na zaklade toho sa vypisu vysledky do zoznamov.
	 */
    private void showResults(OntologyEvaluator oe) {        
        final List<DealComponent> componentsToShowLeft = new ArrayList<>();
        final List<DealComponent> componentsToShowRight = new ArrayList<>();
        
        if(retainedCheckBox.isSelected()){
            componentsToShowLeft.addAll(twoOntologiesResults.getListOfRetainedOldComponents());
            componentsToShowRight.addAll(twoOntologiesResults.getListOfRetainedOldComponents());            
        }
      
        if (twoOntologiesResults != null) {
            if (newCheckBox.isSelected()) {
                componentsToShowRight.addAll(twoOntologiesResults.getListOfNewComponents());
            }
        } else {
            if (newCheckBox.isSelected()) {
                componentsToShowRight.addAll(ontologyEvaluator.getNewComponents());
            }
        }

        if(deletedCheckBox.isSelected()){
            componentsToShowLeft.addAll(twoOntologiesResults.getListOfDeletedComponents());
        }
        if(changedCheckBox.isSelected()){
            for(DealComponentPair cp : twoOntologiesResults.getListOfGoodChangedComponents()){
                componentsToShowLeft.add(cp.getComponent1());
                componentsToShowRight.add(cp.getComponent2());
            }
            for (DealComponentPair cp : twoOntologiesResults.getListOfAdverseChangedComponents()) {
                componentsToShowLeft.add(cp.getComponent1());
                componentsToShowRight.add(cp.getComponent2());
            }
        }
        if (grammarCheckBox.isSelected()) {
            for (ElementResultsWrapper el : oneOntologyResults.getListOfGrammaticalErrors()) {
                if (!componentsToShowRight.contains((DealComponent) el.getElement())) {
                    componentsToShowRight.add((DealComponent) el.getElement());
                }
            }
        }
        if (parentsCheckBox.isSelected()) {
            for (ElementResultsWrapper el : oneOntologyResults.getListOfWrongParents()) {
                if (!componentsToShowRight.contains((DealComponent) el.getElement())) {
                    componentsToShowRight.add((DealComponent) el.getElement());
                }
            }
        }

        originalOntologyList.setModel(new AbstractListModel() {
            @Override
            public int getSize() {
                return componentsToShowLeft.size();
            }

            @Override
            public Object getElementAt(int index) {
                return componentsToShowLeft.get(index);
            }
        });
        originalOntologyList.setCellRenderer(new OntologyListRenderer(null));
        leftScrollPane.setViewportView(originalOntologyList);
        
        newOntologyList.setModel(new AbstractListModel() {
            @Override
            public int getSize() {
                return componentsToShowRight.size();
            }

            @Override
            public Object getElementAt(int index) {
                return componentsToShowRight.get(index);
            }
        });
        newOntologyList.setCellRenderer(new OntologyListRenderer(oe));
        rightScrollPane.setViewportView(newOntologyList);
        
        int metrics = duMetricsCalculator.calculateDuMetrics(twoOntologiesResults, simpleTwoOntologiesResults, oneOntologyResults, adueResults, componentsToShowRight);
        String interpretation = duMetricsCalculator.getMetricsInterpretation();
        this.duScoreValueLabel.setText(metrics + "% (" + interpretation + ")");
    }

    /**
	 * Zobrazenie vysledkov hodnotenia v zoznamoch, podla zvolenych parametrov
	 * (zaskrtnute checkboxy).
	 * 
	 * @param soe
	 *            objekt reprezentujuci akym sposobom boli ontologie hodnotene.
	 *            Na zaklade toho sa vypisu vysledky do zoznamov.
	 */
	private void showResults(SimpleOntologyEvaluator soe) {
		final List<DealElement> elementsToShowLeft = new ArrayList<>();
		final List<DealElement> elementsToShowRight = new ArrayList<>();

		if (retainedCheckBox.isSelected()) {
			elementsToShowLeft.addAll(simpleTwoOntologiesResults.getListOfRetainedOldElements());
			elementsToShowRight.addAll(simpleTwoOntologiesResults.getListOfRetainedOldElements());
		}

		if (simpleTwoOntologiesResults != null) {
			if (newCheckBox.isSelected()) {
				elementsToShowRight.addAll(simpleTwoOntologiesResults.getListOfNewElements());
			}
		} else {
			if (newCheckBox.isSelected()) {
				elementsToShowRight.addAll(simpleOntologyEvaluator.getNewElements());
			}
		}

		if (deletedCheckBox.isSelected()) {
			elementsToShowLeft.addAll(simpleTwoOntologiesResults.getListOfDeletedElements());
		}
		if (!newCheckBox.isSelected() && grammarCheckBox.isSelected()) {
			for (ElementResultsWrapper el : oneOntologyResults.getListOfGrammaticalErrors()) {
				if (!elementsToShowRight.contains((DealElement) el.getElement())) {
					elementsToShowRight.add((DealElement) el.getElement());
				}
			}
		}
		if (!newCheckBox.isSelected() && parentsCheckBox.isSelected()) {
			for (ElementResultsWrapper el : oneOntologyResults.getListOfWrongParents()) {
				if (!elementsToShowRight.contains((DealElement) el.getElement())) {
					elementsToShowRight.add((DealElement) el.getElement());
				}
			}
		}

		originalOntologyList.setModel(new AbstractListModel() {
			@Override
			public int getSize() {
				return elementsToShowLeft.size();
			}

			@Override
			public Object getElementAt(int index) {
				return elementsToShowLeft.get(index);
			}
		});
		originalOntologyList.setCellRenderer(new OntologyListRenderer(null));
		leftScrollPane.setViewportView(originalOntologyList);

		newOntologyList.setModel(new AbstractListModel() {
			@Override
			public int getSize() {
				return elementsToShowRight.size();
			}

			@Override
			public Object getElementAt(int index) {
				return elementsToShowRight.get(index);
			}
		});
		newOntologyList.setCellRenderer(new OntologyListRenderer(soe));
		rightScrollPane.setViewportView(newOntologyList);

	}

	/**
	 * Naplnenie stavovej tabulky porvkov ontologie, informaciami zo oznacenych
	 * poloziek v zoznamoch.
	 * 
	 * @param left
	 *            polozka reprezentujuca oznacenu polozku v lavom zozname.
	 * @param right
	 *            polozka reprezentujuca oznacenu polozku v pravom zozname.
	 */
	private void populateTable(AbstractElement left, AbstractElement right) {
		DefaultTableModel model = new DefaultTableModel();

		if (left instanceof DealComponent || right instanceof DealComponent) {
			for (String columnName : COLUMN_NAMES_LONG_EVALUATOR) {
				model.addColumn(columnName);
			}
			// Rows: "ID", "Class", "Text", "Parent", "Childs"
			// Columns: "OLD","NEW","DELETED","CHANGED","GRAMMAR","PARENTS"
			DealComponent leftComponent = (DealComponent) left;
			DealComponent rightComponent = (DealComponent) right;
			Object[][] data = new Object[5][COLUMN_NAMES_LONG_EVALUATOR.length]; // [row][column]

			if (twoOntologiesResults == null) {
				data[0][1] = rightComponent.getId();
				data[1][1] = rightComponent.getComponentClass();
				data[2][1] = rightComponent.getText();
				data[3][1] = rightComponent.getParent();
				data[4][1] = rightComponent.getChilds();
			} else {
				if (leftComponent != null && rightComponent != null) {
					// Ak retainedComponent
					if (twoOntologiesResults.getListOfRetainedOldComponents().contains(leftComponent) && twoOntologiesResults.getListOfRetainedOldComponents().contains(rightComponent)) {
						data[0][0] = rightComponent.getId();
						data[1][0] = rightComponent.getComponentClass();
						data[2][0] = rightComponent.getText();
						data[3][0] = rightComponent.getParent();
						data[4][0] = rightComponent.getChilds();
					}
					// Ak changedComponent
					if (twoOntologiesResults.getListOfGoodChangedComponents().contains(new DealComponentPair(leftComponent, rightComponent)) || twoOntologiesResults.getListOfAdverseChangedComponents().contains(new DealComponentPair(leftComponent, rightComponent))) {
						// stare
						data[0][0] = leftComponent.getId();
						data[1][0] = leftComponent.getComponentClass();
						data[2][0] = leftComponent.getText();
						data[3][0] = leftComponent.getParent();
						data[4][0] = leftComponent.getChilds();
						// zmenene
						data[0][3] = rightComponent.getId();
						data[1][3] = rightComponent.getComponentClass();
						data[2][3] = rightComponent.getText();
						data[3][3] = rightComponent.getParent();
						data[4][3] = rightComponent.getChilds();
					}
				} else if (leftComponent != null) {
					// Ak deletedComponent
					if (twoOntologiesResults.getListOfDeletedComponents().contains(leftComponent)) {
						data[0][2] = leftComponent.getId();
						data[1][2] = leftComponent.getComponentClass();
						data[2][2] = leftComponent.getText();
						data[3][2] = leftComponent.getParent();
						data[4][2] = leftComponent.getChilds();
					}
				} else if (rightComponent != null) {
					// Ak newComponent
					if (twoOntologiesResults.getListOfNewComponents().contains(rightComponent)) {
						data[0][1] = rightComponent.getId();
						data[1][1] = rightComponent.getComponentClass();
						data[2][1] = rightComponent.getText();
						data[3][1] = rightComponent.getParent();
						data[4][1] = rightComponent.getChilds();
					}
				}
			}

			for (int row = 0; row < DealComponent.getPropertyNames().length; row++) {
				model.addRow(data[row]);
			}
			table.setModel(model);
			tableScrollPane.setRowHeaderView(new RowHeaderTable(DealComponent.getPropertyNames()));
		} else if (left instanceof DealElement || right instanceof DealElement) {
			for (String columnName : COLUMN_NAMES_SIMPLE_EVALUATOR) {
				model.addColumn(columnName);
			}
			// Rows: "ID", "Name", "Class", "Label", "Description", "Parent",
			// "Childs"
			// Columns: "OLD","NEW","DELETED","CHANGED","GRAMMAR","PARENTS"
			DealElement leftElement = (DealElement) left;
			DealElement rightElement = (DealElement) right;
			Object[][] data = new Object[7][COLUMN_NAMES_SIMPLE_EVALUATOR.length]; // [row][column]

			if (simpleTwoOntologiesResults == null) {
				data[0][1] = rightElement.getId();
				data[1][1] = rightElement.getName();
				data[2][1] = rightElement.getComponentClass();
				data[3][1] = rightElement.getLabel();
				data[4][1] = rightElement.getDescription();
				data[5][1] = rightElement.getParent();
				data[6][1] = rightElement.getChilds();
			} else {
				if (leftElement != null && rightElement != null) {
					// Ak retaindedElement
					if (simpleTwoOntologiesResults.getListOfRetainedOldElements().contains(leftElement) && simpleTwoOntologiesResults.getListOfRetainedOldElements().contains(rightElement)) {
						data[0][0] = rightElement.getId();
						data[1][0] = rightElement.getName();
						data[2][0] = rightElement.getComponentClass();
						data[3][0] = rightElement.getLabel();
						data[4][0] = rightElement.getDescription();
						data[5][0] = rightElement.getParent();
						data[6][0] = rightElement.getChilds();
					}
				} else if (leftElement != null) {
					// Ak deletedElement
					if (simpleTwoOntologiesResults.getListOfDeletedElements().contains(leftElement)) {
						data[0][2] = leftElement.getId();
						data[1][2] = leftElement.getName();
						data[2][2] = leftElement.getComponentClass();
						data[3][2] = leftElement.getLabel();
						data[4][2] = leftElement.getDescription();
						data[5][2] = leftElement.getParent();
						data[6][2] = leftElement.getChilds();
					}
				} else if (rightElement != null) {
					// Ak newElement
					if (simpleTwoOntologiesResults.getListOfNewElements().contains(rightElement)) {
						data[0][1] = rightElement.getId();
						data[1][1] = rightElement.getName();
						data[2][1] = rightElement.getComponentClass();
						data[3][1] = rightElement.getLabel();
						data[4][1] = rightElement.getDescription();
						data[5][1] = rightElement.getParent();
						data[6][1] = rightElement.getChilds();
					}
				}
			}

			for (int row = 0; row < DealElement.getPropertyNames().length; row++) {
				model.addRow(data[row]);
			}
			table.setModel(model);
			tableScrollPane.setRowHeaderView(new RowHeaderTable(DealElement.getPropertyNames()));
		}
		TableColumnModel colModel = table.getColumnModel();
		for (int i = 0; i < colModel.getColumnCount(); i++) {
			colModel.getColumn(i).setCellRenderer(new DefaultTableCellRenderer() {
				@Override
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					if (table.getValueAt(row, column) != null) {
						setToolTipText((table.getValueAt(row, column)).toString());
					}
					return this;
				}
			});
		}
		populateSuggestionsTable(right);
	}

	/**
	 * Naplnenie tabulky reprezentujucej gramaticke chyby a zle definovanych
	 * rodicov.
	 * 
	 * @param element
	 *            polozka podla ktorej maju byt naplnene udaje do tabulky.
	 */
	private void populateSuggestionsTable(AbstractElement element) {
		DefaultTableModel model = new DefaultTableModel();
		for (String columnName : COLUMN_NAMES_OF_SUGGESTIONS) {
			model.addColumn(columnName);
		}
		Object[][] data;
		if (element instanceof DealComponent) {
			data = new Object[5][COLUMN_NAMES_OF_SUGGESTIONS.length];
			ElementResultsWrapper erw = oneOntologyResults.getElementWithGrammaticalErrorsByElement(element);
			if (erw != null) {
				data[2][0] = erw.getCorrection();
			}
			erw = oneOntologyResults.geteElementWithWrongParentsByElement(element);
			if (erw != null) {
				data[0][1] = erw.getCorrection();
			}
			for (int row = 0; row < DealComponent.getPropertyNames().length; row++) {
				model.addRow(data[row]);
			}
		} else if (element instanceof DealElement) {
			data = new Object[7][COLUMN_NAMES_OF_SUGGESTIONS.length];
			ElementResultsWrapper erw = oneOntologyResults.getElementWithGrammaticalErrorsByElement(element);
			if (erw != null) {
				DealElement de = (DealElement) element;
				if (de.getLabel() != null && !"".equals(de.getLabel())) {
					data[3][0] = erw.getCorrection();
				} else {
					data[1][0] = erw.getCorrection();
				}
			}
			erw = oneOntologyResults.geteElementWithWrongParentsByElement(element);
			if (erw != null) {
				data[0][1] = erw.getCorrection();
			}
			for (int row = 0; row < DealElement.getPropertyNames().length; row++) {
				model.addRow(data[row]);
			}
		}
		suggestionsTable.setModel(model);
		TableColumnModel colModel = suggestionsTable.getColumnModel();
		for (int i = 0; i < colModel.getColumnCount(); i++) {
			colModel.getColumn(i).setCellRenderer(new DefaultTableCellRenderer() {
				@Override
				public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
					super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					if (table.getValueAt(row, column) != null) {
						setToolTipText((table.getValueAt(row, column)).toString());
					}
					return this;
				}
			});
		}
	}

	/**
	 * Pred spustenim hodnotenia je potrebne vsetko resetovat/nastavit na null.
	 * Je to z dovodu, aby sa nestalo ze sa zobrazia nejake stare vysledky.
	 */
	private void clearLastEvaluation(){
        ontologyEvaluator = null;
        simpleOntologyEvaluator = null;
        twoOntologiesResults = null;
        simpleTwoOntologiesResults = null;
        oneOntologyResults = null;
        
        originalOntologyList.setModel(new AbstractListModel() {
            @Override
            public int getSize() {
                return 0;
            }

            @Override
            public Object getElementAt(int index) {
                return null;
            }
        });
        originalOntologyList.setCellRenderer(new OntologyListRenderer(null));
        leftScrollPane.setViewportView(originalOntologyList);
        
        newOntologyList.setModel(new AbstractListModel() {
            @Override
            public int getSize() {
                return 0;
            }

            @Override
            public Object getElementAt(int index) {
                return null;
            }
        });
        newOntologyList.setCellRenderer(new OntologyListRenderer(null));
        rightScrollPane.setViewportView(newOntologyList);
        
        if (table.getModel() != null) {
            ((DefaultTableModel)table.getModel()).setRowCount(0);
        }
        if(suggestionsTable.getModel() != null){
            ((DefaultTableModel)suggestionsTable.getModel()).setRowCount(0);
        }
    }
    
    /**
     * Vseko interaktivne sa na stavi bud na enable alebo disable.
     * Je to potrebne pocas vykonavania procesu hodnotenia.
     * Povolenie checkboxov je ale zvlast metoda setEnabledCheckBoxes
     * @param value 
     */
	private void setEnabledTool(boolean value) {
		loadNewOntologyButton.setEnabled(value);
		evaluateOntologyButton.setEnabled(value);
		loadOriginalOntologyButton.setEnabled(value);
		evaluationTypeComboBox.setEnabled(value);
		tableScrollPane.setEnabled(value);
		table.setEnabled(value);
		suggestionsTable.setEnabled(value);
		leftScrollPane.setEnabled(value);
		newOntologyList.setEnabled(value);
		originalOntologyList.setEnabled(value);
		rightScrollPane.setEnabled(value);
		setDictionaryButton.setEnabled(value);
		if (value && this.oldOntologyPath != null) {
			fullCheckCheckBox.setEnabled(value);
		}
		if (!value) {
			setEnabledCheckBoxes(value);
		}
	}

	/**
	 * Metoda, ktora podla paramtera nastavi stavove checkboxy na true alebo
	 * false. Podla nich sa potom vypisuju polozky do zoznamov.
	 * 
	 * @param value
	 *            true ak sa maju polozky nastavit na pristupne, v opacnom
	 *            pripade false.
	 */
	private void setEnabledCheckBoxes(boolean value) {
		if (!value) {
			retainedCheckBox.setSelected(value);
			newCheckBox.setSelected(value);
			deletedCheckBox.setSelected(value);
			changedCheckBox.setSelected(value);
			grammarCheckBox.setSelected(value);
			parentsCheckBox.setSelected(value);

			retainedCheckBox.setEnabled(value);
			newCheckBox.setEnabled(value);
			deletedCheckBox.setEnabled(value);
			changedCheckBox.setEnabled(value);
			grammarCheckBox.setEnabled(value);
			parentsCheckBox.setEnabled(value);
		} else {
			if (twoOntologiesResults == null && simpleTwoOntologiesResults == null) {
				newCheckBox.setEnabled(value);
				newCheckBox.setSelected(value);//
				grammarCheckBox.setEnabled(value);
				grammarCheckBox.setSelected(value);//
				parentsCheckBox.setEnabled(value);
				parentsCheckBox.setSelected(value);//
			} else {
				retainedCheckBox.setEnabled(value);
				retainedCheckBox.setSelected(value);//
				newCheckBox.setEnabled(value);
				newCheckBox.setSelected(value);//
				deletedCheckBox.setEnabled(value);
				deletedCheckBox.setSelected(value);//
				if (simpleOntologyEvaluator == null) {
					changedCheckBox.setEnabled(value);
					changedCheckBox.setSelected(value);//
				}
				grammarCheckBox.setEnabled(value);
				grammarCheckBox.setSelected(value);//
				parentsCheckBox.setEnabled(value);
				parentsCheckBox.setSelected(value);//
			}
		}
	}
    
	/**
	 * Metoda volajuca metodu na vypisanie vysledkov do zoznamov, podla
	 * objektu/sposobu hodnotenia ontologii.
	 */
	private void someCheckBoxChanged() {
		if (ontologyEvaluator != null) {
			showResults(ontologyEvaluator);
		} else if (simpleOntologyEvaluator != null) {
			showResults(simpleOntologyEvaluator);
		}
	}
    
	/**
	 * Metoda spustajuca hodnotenie na zaklade zvoleneho parametra. Vsetky
	 * predosle vysledky resetuje, vytvori novy objekt evaluatora na hodnotenie,
	 * a spusti hodnotenie na pozadi. Po skonceni procesu hodnotenia zavola
	 * metodu na zobrazenie vysledkov.
	 * 
	 * @param oe
	 *            objekt reprezentujuci sposob hodnotenia.
	 */
	private void startEvaluatingOnBackground(OntologyEvaluator oe) {
		UIManager.put("nimbusOrange", Color.ORANGE);
		progressBar.setValue(0);
		clearLastEvaluation();
		try {
			if (oldOntologyPath == null) {
				ontologyEvaluator = new OntologyEvaluator(newOntologyPath, fullCheckCheckBox.isSelected());
			} else {
				ontologyEvaluator = new OntologyEvaluator(oldOntologyPath, newOntologyPath, fullCheckCheckBox.isSelected());
			}
			ontologyEvaluator.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					switch (evt.getPropertyName()) {
					case "progress":
						progressBar.setIndeterminate(false);
						progressBar.setValue((Integer) evt.getNewValue());
						break;
					case "state":
						switch ((StateValue) evt.getNewValue()) {
						case DONE:
							UIManager.put("nimbusOrange", Color.GREEN);
							twoOntologiesResults = ontologyEvaluator.getTwoOntologiesResults();
							simpleTwoOntologiesResults = null;
							oneOntologyResults = ontologyEvaluator.getOneOntologyResults();
							setEnabledCheckBoxes(true);
							showResults(ontologyEvaluator);
							showTableTemplate(ontologyEvaluator);
							fillStateLabel();
							setEnabledTool(true);
							endTime = System.currentTimeMillis();
							System.out.println("Started: " + new Date(startTime) + " End: " + new Date(endTime) + " Duration: " + new Time(endTime - startTime));
							break;
						case STARTED:
							startTime = System.currentTimeMillis();
							setEnabledTool(false);
						case PENDING:
							progressBar.setVisible(true);
							progressBar.setIndeterminate(true);
							break;
						}
						break;
					}
				}
			});
			ontologyEvaluator.execute();
		} catch (OWLOntologyCreationException ex) {
			Logger.getLogger(ADUEtool_main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Metoda spustajuca hodnotenie na zaklade zvoleneho parametra. Vsetky
	 * predosle vysledky resetuje, vytvori novy objekt evaluatora na hodnotenie,
	 * a spusti hodnotenie na pozadi. Po skonceni procesu hodnotenia zavola
	 * metodu na zobrazenie vysledkov.
	 * 
	 * @param soe
	 *            objekt reprezentujuci sposob hodnotenia.
	 */
	private void startEvaluatingOnBackground(SimpleOntologyEvaluator soe) {
		UIManager.put("nimbusOrange", Color.ORANGE);
		progressBar.setValue(0);
		clearLastEvaluation();
		try {
			if (oldOntologyPath == null) {
				simpleOntologyEvaluator = new SimpleOntologyEvaluator(newOntologyPath, fullCheckCheckBox.isSelected());
			} else {
				simpleOntologyEvaluator = new SimpleOntologyEvaluator(oldOntologyPath, newOntologyPath, fullCheckCheckBox.isSelected());
			}
			simpleOntologyEvaluator.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					switch (evt.getPropertyName()) {
					case "progress":
						progressBar.setIndeterminate(false);
						progressBar.setValue((Integer) evt.getNewValue());
						break;
					case "state":
						switch ((StateValue) evt.getNewValue()) {
						case DONE:
							UIManager.put("nimbusOrange", Color.GREEN);
							twoOntologiesResults = null;
							simpleTwoOntologiesResults = simpleOntologyEvaluator.getSimpleTwoOntologiesResults();
							oneOntologyResults = simpleOntologyEvaluator.getSimpleOneOntologyResults();
							setEnabledCheckBoxes(true);
							showResults(simpleOntologyEvaluator);
							showTableTemplate(simpleOntologyEvaluator);
							fillStateLabel();
							setEnabledTool(true);
							endTime = System.currentTimeMillis();
							System.out.println("Started: " + new Date(startTime) + " End: " + new Date(endTime) + " Duration: " + new Time(endTime - startTime));
							break;
						case STARTED:
							startTime = System.currentTimeMillis();
							setEnabledTool(false);
						case PENDING:
							progressBar.setVisible(true);
							progressBar.setIndeterminate(true);
							break;
						}
						break;
					}
				}
			});
			simpleOntologyEvaluator.execute();
		} catch (OWLOntologyCreationException ex) {
			Logger.getLogger(ADUEtool_main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Metoda na zobrazenie struktury tabulky pre zobrazenie informacii o
	 * jednotlivych polozkach v zoznamoch.
	 * 
	 * @param oe
	 *            objekt reprezentujuci sposob hodnotenia. Na zaklade sposobu
	 *            hodnotenia su dostupne odlisne informacie o polozkach.
	 */
	private void showTableTemplate(OntologyEvaluator oe) {
		DefaultTableModel model = new DefaultTableModel();
		for (String columnName : COLUMN_NAMES_LONG_EVALUATOR) {
			model.addColumn(columnName);
		}
		table.setModel(model);
		tableScrollPane.setRowHeaderView(new RowHeaderTable(DealComponent.getPropertyNames()));
		showSuggestionsTableTemplate();
	}

	/**
	 * Metoda na zobrazenie struktury tabulky pre zobrazenie informacii o
	 * jednotlivych polozkach v zoznamoch.
	 * 
	 * @param soe
	 *            objekt reprezentujuci sposob hodnotenia. Na zaklade sposobu
	 *            hodnotenia su dostupne odlisne informacie o polozkach.
	 */
	private void showTableTemplate(SimpleOntologyEvaluator soe) {
		DefaultTableModel model = new DefaultTableModel();
		for (String columnName : COLUMN_NAMES_SIMPLE_EVALUATOR) {
			model.addColumn(columnName);
		}
		table.setModel(model);
		tableScrollPane.setRowHeaderView(new RowHeaderTable(DealElement.getPropertyNames()));
		showSuggestionsTableTemplate();
	}
    
	/**
	 * Metoda na zobrazenie struktury tabulky pre zobrazenie informacii o
	 * vysledku hodnotenia gramatiky a overovania spravneho pojmu rodica.
	 */
	private void showSuggestionsTableTemplate() {
		DefaultTableModel suggestionsModel = new DefaultTableModel();
		for (String columnName : COLUMN_NAMES_OF_SUGGESTIONS) {
			suggestionsModel.addColumn(columnName);
		}
		suggestionsTable.setModel(suggestionsModel);
	}
    
	/**
	 * Naplnenie stavoveho riadku hodnotami reprezentujucimi vysledok hodnotenia.
	 */
    private void fillStateLabel() {
        if (twoOntologiesResults != null) {
            statusRetainedLabel.setText(twoOntologiesResults.getListOfRetainedOldComponents().size() + " retained");
            statusNewLabel.setText(twoOntologiesResults.getListOfNewComponents().size() + " new");
            statusDeletedLabel.setText(twoOntologiesResults.getListOfDeletedComponents().size() + " deleted");
            statusChangedLabel.setText(twoOntologiesResults.getListOfAdverseChangedComponents().size() + twoOntologiesResults.getListOfGoodChangedComponents().size() + " changed");
            statusGrammarLabel.setText(oneOntologyResults.getListOfGrammaticalErrors().size() + " grammar error");
            statusParentLabel.setText(oneOntologyResults.getListOfWrongParents().size() + " wrong parent");
            counts(ontologyEvaluator);
        } else if (simpleTwoOntologiesResults != null) {            
            statusRetainedLabel.setText(simpleTwoOntologiesResults.getListOfRetainedOldElements().size() + " retained");
            statusNewLabel.setText(simpleTwoOntologiesResults.getListOfNewElements().size() + " new");
            statusDeletedLabel.setText(simpleTwoOntologiesResults.getListOfDeletedElements().size() + " deleted");
            statusChangedLabel.setText("0 changed");
            statusGrammarLabel.setText(oneOntologyResults.getListOfGrammaticalErrors().size() + " grammar error");
            statusParentLabel.setText(oneOntologyResults.getListOfWrongParents().size() + " wrong parent");
            counts(simpleOntologyEvaluator);            
        } else {
            statusRetainedLabel.setText("0 retained");
            statusNewLabel.setText("0 new");
            if (ontologyEvaluator != null) {
            	counts(ontologyEvaluator);            	
                statusNewLabel.setText(ontologyEvaluator.getNewComponents().size() + " new");
            } else if (simpleOntologyEvaluator != null) {
            	counts(simpleOntologyEvaluator); 
                statusNewLabel.setText(simpleOntologyEvaluator.getNewElements().size() + " new");
            }
            statusDeletedLabel.setText("0 deleted");
            statusChangedLabel.setText("0 changed");
            statusGrammarLabel.setText(oneOntologyResults.getListOfGrammaticalErrors().size() + " grammar error");
            statusParentLabel.setText(oneOntologyResults.getListOfWrongParents().size() + " wrong parent");
        }
    }
       
    
    
    // zmazat len pre ucely testovania!
	private void counts(OntologyEvaluator oe) {
		int novych = oe.getNewComponents().size();
		int starych = 0;
		int ponechanych = 0;
		int nove = 0;
		int zmazane = 0;
		int zmenene = 0;
		int spravneZmenene = 0;
		int zleZmenene = 0;
		if (twoOntologiesResults != null) {
			starych = oe.getOldComponents().size();
			ponechanych = twoOntologiesResults.getListOfRetainedOldComponents().size();
			nove = twoOntologiesResults.getListOfNewComponents().size();
			zmazane = twoOntologiesResults.getListOfDeletedComponents().size();
			zmenene = twoOntologiesResults.getListOfAdverseChangedComponents().size() + twoOntologiesResults.getListOfGoodChangedComponents().size();
			spravneZmenene = twoOntologiesResults.getListOfGoodChangedComponents().size();
			zleZmenene = twoOntologiesResults.getListOfAdverseChangedComponents().size();
		}
		int gramatika = oneOntologyResults.getListOfGrammaticalErrors().size();
		int parent = oneOntologyResults.getListOfWrongParents().size();
		System.out.println("Vstarej = " + starych + "\n" + "Vnovej = " + novych + "\n" + "ponechanych = " + ponechanych + "\n" + "nove = " + nove + "\n" + "zmazane = " + zmazane + "\n" + "zmenene = " + zmenene + "\n"
		+ "         z toho spravne zmenene = " + spravneZmenene + "\n"
		+ "         z toho zle zmenene = " + zleZmenene + "\n"
		+"gramatika = " + gramatika + "\n" + "parent = " + parent + "\n");
	}
	private void counts(SimpleOntologyEvaluator oe) {
		int novych = oe.getNewElements().size();
		int starych = 0;
		int ponechanych = 0;
		int nove = 0;
		int zmazane = 0;		
		if (simpleTwoOntologiesResults != null) {
			starych = oe.getOldElements().size();
			ponechanych = simpleTwoOntologiesResults.getListOfRetainedOldElements().size();
			nove = simpleTwoOntologiesResults.getListOfNewElements().size();
			zmazane = simpleTwoOntologiesResults.getListOfDeletedElements().size();			
		}
		int gramatika = oneOntologyResults.getListOfGrammaticalErrors().size();
		int parent = oneOntologyResults.getListOfWrongParents().size();
		System.out.println("Vstarej = " + starych + "\n" + "Vnovej = " + novych + "\n" + "ponechanych = " + ponechanych + "\n" + "nove = " + nove + "\n" + "zmazane = " + zmazane + "\n" + "gramatika = " + gramatika + "\n" + "parent = " + parent + "\n");
	}
    
}
